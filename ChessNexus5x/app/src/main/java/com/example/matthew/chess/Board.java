package com.example.matthew.chess;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;
import android.widget.ViewFlipper;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Gallery;
import android.widget.BaseAdapter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;


/**
 * This is the class that represents the chess board with an 8x8 matrix
 * of Positions on the board are made. This is also where the pieces starting
 * positions are instantiated.
 * @author Matthew Eder & Alex Weinrich
 */
public class Board extends Thread {
	/**
	 * The matrix that represents the board
	 */
	Position[][] positions = new Position[8][8];
	/**
	 * This is the constructor of the board class. This is where the matrix that
	 * represents the board is filled with Position objects and the starting 
	 * position of the pieces is set
	 * @param player1 the lightsquare player
	 * @param player2 the blacksquare player
	 */
	Activity mActivity;
	Context context;
	ViewFlipper flippy;
	GridView chessboardGridView;
	Toolbar toolbar;
	public BaseAdapter adapter;
	boolean waiting = true;
	static boolean callFromReplay = false;
	static int turnDraw = 0;
	public Board(Player player1, Player player2, Activity mActivity, Context context, ViewFlipper flippy, Toolbar toolbar) {
		this.context = context;
		this.mActivity = mActivity;
		this.flippy = flippy;
		this.toolbar = toolbar;
        for(int i = 0; i < positions.length; i++) {
            for(int j = 0; j < positions[i].length; j++) {
                this.positions[j][i] = new Position(j, i);  
            }
        }
		// lightsquare
		for (int i = 0; i < 8; i++) {
			this.getPosition(i, 6).setPosition(new Pawn(player1));
		}
		this.getPosition(0, 7).setPosition(new Rook(player1));
		this.getPosition(1, 7).setPosition(new Knight(player1));
		this.getPosition(2, 7).setPosition(new Bishop(player1));
		this.getPosition(3, 7).setPosition(new Queen(player1));
		this.getPosition(4, 7).setPosition(new King(player1));
		this.getPosition(5, 7).setPosition(new Bishop(player1));
		this.getPosition(6, 7).setPosition(new Knight(player1));
		this.getPosition(7, 7).setPosition(new Rook(player1));
		// blacksquare
		for (int i = 0; i < 8; i++) {
			this.getPosition(i, 1).setPosition(new Pawn(player2));
		}
		this.getPosition(0, 0).setPosition(new Rook(player2));
		this.getPosition(1, 0).setPosition(new Knight(player2));
		this.getPosition(2, 0).setPosition(new Bishop(player2));
		this.getPosition(3, 0).setPosition(new Queen(player2));
		this.getPosition(4, 0).setPosition(new King(player2));
		this.getPosition(5, 0).setPosition(new Bishop(player2));
		this.getPosition(6, 0).setPosition(new Knight(player2));
		this.getPosition(7, 0).setPosition(new Rook(player2));
    }

    /**
     * @param x the x position corresponds to the letter input 
     * @param y the y position corresponds to the number input
     * @return Position object at the (x, y) coordinates given
     */
    public Position getPosition(int x, int y) {
        return positions[x][y];
    }
    /**
     * Prints the board as described in the project description.
     * Also checks the state of the game and prints check, checkmate or
     * stalemate if it meets that condition.
     * @return The state of the game. If the game is over return true
     */
    public boolean printBoard() {
    	//Piece piece;
    	//Boolean currentPlayer = Game.getCurrentPlayer();
		if(callFromReplay){
			chessboardGridView = (GridView) flippy.findViewById(R.id.gridView2);
			chessboardGridView.setVerticalScrollBarEnabled(false);
			//ViewGroup.LayoutParams layoutParams = chessboardGridView.getLayoutParams();
			//layoutParams.height = Game.screenWidth;
			//chessboardGridView.setLayoutParams(layoutParams);
			//System.out.println("Chessboard height: " + chessboardGridView.getHeight());
			chessboardGridView.setAdapter(adapter = new ImageAdapter(context, flippy, mActivity, chessboardGridView, this));
		} else {
			chessboardGridView = (GridView) flippy.findViewById(R.id.gridView);
			chessboardGridView.setVerticalScrollBarEnabled(false);
			//System.out.println("Chessboard height: " + chessboardGridView.getMeasuredHeight() );
			chessboardGridView.setAdapter(adapter = new ImageAdapter(context, flippy, mActivity, chessboardGridView, this));
		}

		return false;
    }

	public boolean boardChecker() {
		TextView text;
		if (callFromReplay){
			text = (TextView)flippy.findViewById(R.id.replayTextView);
		} else {
			text = (TextView) flippy.findViewById(R.id.textView);
		}
		Boolean currentPlayer = Game.getCurrentPlayer();
		Game.turns++;
		Check c = new Check();
		int gameState = 0;
		// Can be taken out of if, used for test cases. You can't be in mate or check the first fews turns anyway
		if(Game.getTurns() != 0) {
			gameState = c.gameStateCheck(this);
		}
		if(gameState == 1) {
			text.setText("Check ");
			if(Game.getCurrentPlayer())
				text.append("White's move");
			else
				text.append("Black's move");
			return false;
		} else if (gameState == 2) {
			text.setText("Checkmate ");
			for (int i = 0; i < chessboardGridView.getChildCount(); i++) {
				View child = chessboardGridView.getChildAt(i);
				child.setOnDragListener(null);
			}
			if (!Game.getCurrentPlayer())
				text.append("White wins");
			else
				text.append("Black wins");
			if(!callFromReplay) {
				endGamePrompt();
			}

			return true;
		} else if (gameState == 3) {
			text.setText("Stalemate Draw ");
			for (int i = 0; i < chessboardGridView.getChildCount(); i++) {
				View child = chessboardGridView.getChildAt(i);
				child.setOnDragListener(null);
			}
			Game.stalemate = true;
			if(!callFromReplay) {
				endGamePrompt();
			}
			return true;
		}

		if(Game.getCurrentPlayer())
			text.setText("White's move");
		else
			text.setText("Black's move");

		if(Game.drawOffer && (Game.turns != Game.turnDrawWasPressed + 2 )) {
			turnDraw = Game.turns;
			text.append(" draw offered");
		} else {
			Game.turnDrawWasPressed = 0;
		}
		if(Game.turnDrawWasPressed != Game.turns - 1) {
			Game.drawOffer = false;
		}
		Game.drawWasPressed = false;

		return false;
	}
	public void endGamePrompt() {
		final Dialog saveDialoge = new Dialog(mActivity);
		saveDialoge.setTitle("Save Game?");
		saveDialoge.setContentView(R.layout.savegame);
		saveDialoge.setCanceledOnTouchOutside(false);
		saveDialoge.show();
		final EditText gameTitle = (EditText) saveDialoge.findViewById(R.id.gameTitle);
		final Button yes = (Button) saveDialoge.findViewById(R.id.yes);
		final Button no = (Button) saveDialoge.findViewById(R.id.no);
		yes.setEnabled(false);
		gameTitle.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				yes.setEnabled(false);
			}
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				yes.setEnabled(false);
			}
			@Override
			public void afterTextChanged(Editable s) {
				boolean nameExist = false;
				String in = gameTitle.getText().toString();
				for(GameRecordArray GRA: Game.gameList) {
					if(GRA.getTitle().equalsIgnoreCase(in)){
						nameExist = true;
						break;
					}
				}
				if(!nameExist){
					yes.setEnabled(true);
				} else {
					yes.setEnabled(false);
				}
			}
		});

		yes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Game.GRArray.setTitle(gameTitle.getText().toString());
				Game.gameList.add(Game.GRArray);
				try {
					FileOutputStream f_out = context.openFileOutput("games.ser", context.MODE_PRIVATE);
					ObjectOutputStream obj_out = new ObjectOutputStream(f_out);
					obj_out.writeObject(Game.gameList);
					f_out.close();
					obj_out.close();
				} catch (Exception e) {
				}


				flippy.setDisplayedChild(0);
				toolbar.setNavigationIcon(null);
				saveDialoge.dismiss();
			}
		});
		no.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				flippy.setDisplayedChild(0);
				toolbar.setNavigationIcon(null);
				saveDialoge.dismiss();
			}
		});
	}
}


