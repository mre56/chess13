package com.example.matthew.chess;
/**
 * Subclass of Piece contains legal moves for a queen
 * @author Matthew Eder & Alex Weinrich
 */
public class Knight extends Piece {
	/**
	 * Calls super class Piece with player param
	 * @param player that owns the piece
	 */
	public Knight(Player player) {
		super(player);
	}
	/**
	 * Calls Piece isValid first with same params
	 * @param board Board object of the game
 	 * @param fromX the initial x position of the piece
 	 * @param fromY the initial y position of the piece
 	 * @param toX the x position the user inputs
 	 * @param toY the y position the user inputs
	 * @return true if the move is a valid chess move for a knight
	 */
	public boolean isValid(Board board, int fromX, int fromY, int toX, int toY) 
	{
		// Checks if piece owner has piece there, and if its on the board, BorW
		if (!super.isValid(board, fromX, fromY, toX, toY))
			return false;
		/*
		 * 8 "Possible Moves" for Knight: -Up 2 Right 1 -Up 2 Left 1
		 * 
		 * -Up 1 Right 2 -Up 1 Left 2 Add a comment to this line
		 * 
		 * -Down 1 Left 2 -Down 1 Right 2
		 * 
		 * -Down 2 Left 1 -Down 2 Right 1 
		 * All possible moves are encompassed in
		 * these 8 mappings. Movements are identical regardless of color. Can be
		 * bound to two conditions: V change 2 H change 1, V change 1 H change 2
		 */
		// V change 2 H change 1
		if ((Math.abs(fromY - toY) == 2) && (Math.abs(fromX - toX) == 1))
			return true;
		// V change 1 H change 2
		else if ((Math.abs(fromY - toY) == 1) && (Math.abs(fromX - toX) == 2))
			return true;
		// Else its invalid
		else
			return false;
	}
	/**
	 * Calls super toString first to get the string associated with the player
	 * @return prints the knight as shown in project description "Player + 'N'"
	 */
	public String toString() {
		return super.toString() + "n";
	}
}
