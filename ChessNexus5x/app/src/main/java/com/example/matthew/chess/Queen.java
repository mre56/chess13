package com.example.matthew.chess;

import java.util.ArrayList;
/**
 * Subclass of Piece contains legal moves for a queen
 * @author Matthew Eder & Alex Weinrich
 */
public class Queen extends Piece {
	/**
	 * A list of positions a piece would travel through in a valid move  
	 */
	ArrayList<Position>  path = new ArrayList<Position>();
	/**
	 * Calls super class Piece with player param
	 * @param player that owns the piece
	 */
	public Queen(Player player) {
		super(player);
	}
	/**
	 * @return the path the piece would take on a valid move in an ArrayList of positions
	 */
	public ArrayList<Position> getPath() {		
		return path;
	}
    /**
	 * Calls Piece isValid first with same params
	 * @param board Board object of the game
 	 * @param fromX the initial x position of the piece
 	 * @param fromY the initial y position of the piece
 	 * @param toX the x position the user inputs
 	 * @param toY the y position the user inputs
	 * @return true if the move is a valid chess move for a rook
	 */
	public boolean isValid(Board board, int fromX, int fromY, int toX, int toY) {
		if (!super.isValid(board, fromX, fromY, toX, toY)) {
			return false;
		}
		path.clear();
		// code copied from King
		// moves only one space diagonally, vertically, horizontally
		if ((fromX + 1 == toX && fromY + 1 == toY) || (fromX - 1 == toX && fromY - 1 == toY) ||
		    (fromX + 1 == toX && fromY - 1 == toY) || (fromX - 1 == toX && fromY + 1 == toY) ||
		    (fromX == toX && fromY + 1 == toY)     || (fromX == toX && fromY - 1 == toY)     ||
		    (fromX + 1 == toX && fromY == toY)     || (fromX - 1 == toX && fromY == toY)) {
			return true;
		}
		// code copied from rook
		// moves vertically
		if (fromX == toX) {
			if (Math.abs(fromY - toY) != 1) {
				// move down
				if (fromY > toY) {
					for (int i = fromY - 1; i > toY; i--) {
						if (board.getPosition(toX, i).isOccupied()) {
							path.clear();
							return false;
						}
						path.add(board.getPosition(toX, i));
					}
					// move up
				} else {
					for (int i = fromY + 1; i < toY; i++) {
						if (board.getPosition(toX, i).isOccupied()) {
							path.clear();
							return false;
						}
						path.add(board.getPosition(toX, i));
					}
				}
			}
			return true;
		}
		// moves horizontally
		if (fromY == toY) {
			if (Math.abs(fromX - toX) != 1) {
				// move left
				if (fromX > toX) {
					for (int i = fromX - 1; i > toX; i--) {
						if (board.getPosition(i, toY).isOccupied()) {
							path.clear();
							return false;
						}
						path.add(board.getPosition(i, toY));
					}
					// move right
				} else {
					for (int i = fromX + 1; i < toX; i++) {
						if (board.getPosition(i, toY).isOccupied()) {
							path.clear();
							return false;
						}
						path.add(board.getPosition(i, toY));
					}
				}
			}
			return true;
		}
		// code copied from bishop
		// moves multiple spaces diagonally
		if (Math.abs(toX - fromX) == Math.abs(toY - fromY)) {
			// move up
			if (toX > fromX) {
				// move up and right
				if (toY > fromY) {
					for (int i = 1; i < toY - fromY; i++) {
						if (board.getPosition(fromX + i, fromY + i).isOccupied()) {
							path.clear();
							return false;
						}
						path.add(board.getPosition(fromX + i, fromY + i));
					}
					return true;
				// move up and left
				} else {
					for (int i = 1; i < fromY - toY; i++) {
						if (board.getPosition(fromX + i, fromY - i).isOccupied()) {
							path.clear();
							return false;
						}
						path.add(board.getPosition(fromX + i, fromY - i));
					}
					return true;
				}
				// move down
			} else {
				// move down and right
				if (toY > fromY) {
					for (int i = 1; i < toY - fromY; i++) {
						if (board.getPosition(fromX - i, fromY + i).isOccupied()) {
							path.clear();
							return false;
						}
						path.add(board.getPosition(fromX - i, fromY + i));
					}
					return true;
					// move down and left
				} else {
					for (int i = 1; i < fromY - toY; i++) {
						if (board.getPosition(fromX - i, fromY - i).isOccupied()) {
							path.clear();
							return false;
						}
						path.add(board.getPosition(fromX - i, fromY - i));
					}
					return true;
				}
			}
		}
		return false;
	}
	/**
	 * Calls super toString first to get the string associated with the player
	 * @return prints the bishop as shown in project description "Player + 'Q'"
	 */
	public String toString() {
		return super.toString() + "q";
	}
}
