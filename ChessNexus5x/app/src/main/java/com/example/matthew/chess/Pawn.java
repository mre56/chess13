package com.example.matthew.chess;

import android.app.Dialog;
import android.content.ClipData;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/** Pawn Class that contains all the functionality for the Pawn piece,
 i.e. affirming a move is valid, enPassanting, promotion, and toString.
 * 
 * 
 * @author Alex Weinrich
 * @author Matt Eder
 */
public class Pawn extends Piece {
    
    /** True if this pawn has made a double move last turn, false if not.*/
    boolean enPassant = false;
    /** sees if isValid is called for check */ 
    boolean callFromCheck = false;
    /** This is the only constructor for Pawn, takes in a player field and
     uses the Piece constructor
     * @param player determines the owning player of the pawn piece*/
    public Pawn(Player player) {
		super(player);
	}

    /** isValid checks whether the pawn is moving to a correct location or not.
     Checks the following valid moves:
     -Double move on first turn
     -Single move up
     -Attacking diagonally one space
     -Promoting through attacking or single step
     This method uses separate checks for each color and bases it on starting Y.
     * @return returns true if the pawn is attempting a valid move, false otherwise
     * @param board gives the current board to pawn
     * @param fromX the original X position of the pawn
     * @param fromY the original Y position of the pawn
     * @param toX the X destination of the pawn
     * @param toY the Y destination of the pawn*/
    @Override
    public boolean isValid(Board board, int fromX, int fromY, int toX, int toY)
    {
        //Checks if piece owner has piece there, and if its on the board, BorW
        if(!super.isValid(board, fromX, fromY, toX, toY))
            return false;
        //Resetting enPassant if the pawn has moved past the two jump
        if (((fromY!=4) && (board.getPosition(fromX, fromY).getPiece().getPlayer().getColor()))
         || ((fromY!=3) && (!board.getPosition(fromX, fromY).getPiece().getPlayer().getColor())))
        {
            enPassant = false;
        }
        //IF ITS WHITE
        if (board.getPosition(fromX, fromY).getPiece().getPlayer().getColor())
        {
            //IF MOVING TWO UP, NONE OVER, AND FIRST MOVE
            if ((toY==4) && (fromX-toX==0) && (fromY==6))
            {
                //IF ONE OF BLACK'S PIECES IS IN TWO
                if (board.getPosition(toX, toY).isOccupied())
                    return false;
                //IF A PIECE IS IN ONE
                else if (board.getPosition(fromX, 5).isOccupied())
                    return false;
                //ELSE YOURE GOOD
                enPassant = true;
                return true;
            }
            //IF MOVING ONE UP ONE OVER
            else if ((fromY-toY==1) && (Math.abs(fromX-toX)==1))
            {
                //CHECK EN PASSANT STATUS, CHECK SPACE ADJACENT TO MOVE
                if (board.getPosition(toX, fromY).isOccupied())
                    //IF ADJACENT PIECE IS PAWN
                    if (board.getPosition(toX, fromY).getPiece() instanceof Pawn)
                        //IF ITS BLACK
                        if (!board.getPosition(toX, fromY).getPiece().getPlayer().getColor())
                            //IF ITS EN PASSANTABLE
                            if (((Pawn) board.getPosition(toX, fromY).getPiece()).isEnPassantable())
                            {
                            	if(!callFromCheck){
                                    ReadInput.captured = board.getPosition(toX, fromY).clearPosition();
                                    ReadInput.wasEnpassant = true;
                                    ReadInput.prevMoveWasPromote = false;
                                    GridView gridView;
                                    if(ReadInput.callfromReplay) {
                                        gridView = (GridView) ReadInput.flippy.findViewById(R.id.gridView2);
                                    } else {
                                        gridView = (GridView) ReadInput.flippy.findViewById(R.id.gridView);
                                    }
                                    RelativeLayout container = (RelativeLayout) gridView.getChildAt((toX) + ((fromY) * 8));
                                    container.removeView(container.getChildAt(1));
                            	}
                                return true;
                            }
                //ELSE NO EN PASSANT, CHECK FOR BLACK PIECE
                return board.getPosition(toX,toY).isOccupied();
            }
            //IF JUST MOVING UP ONE
            else if ((fromY-toY==1) && (fromX-toX==0))
            {
                return !board.getPosition(toX,toY).isOccupied();
            }
            //ELSE ITS INVALID
            return false;
        }
        //ELSE ITS BLACK SO DO OTHER THING
        else{
            //IF MOVING TWO DOWN, NONE OVER, AND FIRST MOVE
            if ((toY==3) && (fromX-toX==0) && (fromY==1))
            {
                //IF ONE OF WHITE'S PIECES IS IN TWO
                if (board.getPosition(toX, toY).isOccupied())
                    return false;
                //IF A PIECE IS IN ONE
                else if (board.getPosition(fromX, 2).isOccupied())
                    return false;
                //ELSE YOURE GOOD
                enPassant = true;
                return true;
            }
            //IF MOVING ONE DOWN ONE OVER
            else if ((toY-fromY==1) && (Math.abs(fromX-toX)==1))
            {
                //CHECK EN PASSANT STATUS, CHECK SPACE ADJACENT TO MOVE
                if (board.getPosition(toX, fromY).isOccupied())
                    //IF ADJACENT PIECE IS PAWN
                    if (board.getPosition(toX, fromY).getPiece() instanceof Pawn)
                        //IF ITS WHITE
                        if (board.getPosition(toX, fromY).getPiece().getPlayer().getColor())
                            //IF ITS EN PASSANTABLE
                            if (((Pawn) board.getPosition(toX, fromY).getPiece()).isEnPassantable())
                            {
                            	if(!callFromCheck){
                            		ReadInput.captured = board.getPosition(toX, fromY).clearPosition();
                                    ReadInput.wasEnpassant = true;
                                    ReadInput.prevMoveWasPromote = false;
                                    GridView gridView;
                                    if(ReadInput.callfromReplay) {
                                        gridView = (GridView) ReadInput.flippy.findViewById(R.id.gridView2);
                                    } else {
                                        gridView = (GridView) ReadInput.flippy.findViewById(R.id.gridView);
                                    }
                                    RelativeLayout container = (RelativeLayout) gridView.getChildAt((toX) + ((fromY) * 8));
                                    container.removeView(container.getChildAt(1));
                            	}
                                return true;
                            }
                //ELSE NO EN PASSANT, CHECK IF THERE A WHITE PIECE THERE
                return board.getPosition(toX,toY).isOccupied();
            }
            //IF JUST MOVING DOWN ONE
            else if ((toY-fromY==1) && (fromX-toX==0))
            {
                return !board.getPosition(toX,toY).isOccupied();
            }
            //ELSE ITS INVALID
            return false;
        }
    }
    /**
     * @param bol set true if called from check class
     */
    public void setCallFromCheck(boolean bol) {
    	callFromCheck = bol;
    }
    /** isEnPassantable() returns the current state of the enPassant boolean
     for this pawn, used to determine if another pawn can En Passant this pawn.
     * 
     * @return returns the current value of enPassant
     */
    public boolean isEnPassantable()
    {
        return enPassant;
    }

    /** toString to indicate the piece type
     * 
     * @return returns p for pawn type piece
     */
    @Override
    public String toString() {
    	return super.toString() + "p"; 
    }
}
