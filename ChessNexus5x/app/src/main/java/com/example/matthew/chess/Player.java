package com.example.matthew.chess;

/**
 * Class used to hold the boolean for the players color and a toString method to
 * print the piece on the board properly 
 * note: originally meant to hold array of pieces that a player had. It was 
 * annoying to maintain but could be useful in check class
 * @author Matthew Eder & Alex Weinrich
 */
public class Player {
	/**
	 * true corresponds to the lightsquare player, blacksquare to false
	 */
	boolean color; // true == lightsquare
	/**
	 * Constructor for the class, sets color equal to the param given
	 * @param color boolean that corresponds to color, true == lightsquare
	 */
	public Player(Boolean color) {
		this.color = color;
	}
	/**
	 * @return boolean that corresponds to color, true == lightsquare
	 */
	public boolean getColor() {
		return color;
	}
	/**
	 * @return string that corresponds to the color of the player
	 */
	public String toString() {
		if (color)
			return " w";
		else
			return " b";
	}

}
