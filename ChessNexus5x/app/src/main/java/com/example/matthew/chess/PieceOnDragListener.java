package com.example.matthew.chess;

import android.content.Context;
import android.view.DragEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;

import android.widget.RelativeLayout;
import android.widget.ViewFlipper;

public class PieceOnDragListener extends Thread implements View.OnDragListener  {
    private Context context;
    private boolean insideOfMe = false;
    GridView chessboardGridView = null;
    ViewFlipper flippy = null;
    Board board;
    public PieceOnDragListener(Context pContext) {
        this.context = pContext;
    }
    public void setGrid(GridView chessboardGridView) {
        this.chessboardGridView = chessboardGridView;
    }
    public void setFlippy(ViewFlipper flippy) {
        this.flippy = flippy;
    }
    public void setBoard(Board board) {
        this.board = board;
    }
    public boolean onDrag(View pV, DragEvent pEvent) {
        final int lEventAction = pEvent.getAction();

        switch(lEventAction) {

            case DragEvent.ACTION_DRAG_STARTED:
                //System.out.println("ACTION_DRAG_STARTED");
                break;
            case DragEvent.ACTION_DRAG_ENTERED:
                insideOfMe = true;
               // System.out.println("ACTION_DRAG_ENTERED");
                break;
            case DragEvent.ACTION_DRAG_EXITED:
                insideOfMe = false;
             //   System.out.println("ACTION_DRAG_EXITED");
                break;
            case DragEvent.ACTION_DROP:
                if (insideOfMe){
                    View view = (View) pEvent.getLocalState();
                    ViewGroup owner = (ViewGroup) view.getParent();
                    RelativeLayout container = (RelativeLayout) pV;
                    int squareSize = Game.screenWidth/8;

                   /* System.out.println("owner: x " +owner.getX()+" y " + owner.getY() );
                    System.out.println("container: x " +container.getX()+" y " + container.getY() );
                    System.out.println("squareSize: " + squareSize);
                    System.out.println(Game.screenWidth - 7*squareSize);
                    System.out.println((Game.screenWidth - 6*squareSize));
                    System.out.println(Game.screenWidth - 5*squareSize);
                    System.out.println((Game.screenWidth - 4*squareSize));
                    System.out.println((Game.screenWidth - 3*squareSize));
                    System.out.println( (Game.screenWidth - 2*squareSize));
                    System.out.println((Game.screenWidth - 1*squareSize));*/
                    int a = 0;
                    int b = 0;
                    if(owner.getX() == 0) {
                        a = 0;
                    } else  if(owner.getX() == (Game.screenWidth - 7*squareSize)) {
                        a = 1;
                    } else if(owner.getX() == (Game.screenWidth - 6*squareSize)) {
                        a = 2;
                    } else  if(owner.getX() == (Game.screenWidth - 5*squareSize)) {
                        a = 3;
                    } else  if(owner.getX() == (Game.screenWidth - 4*squareSize)) {
                        a = 4;
                    } else  if(owner.getX() == (Game.screenWidth - 3*squareSize)) {
                        a = 5;
                    } else  if(owner.getX() == (Game.screenWidth - 2*squareSize)) {
                        a = 6;
                    } else  if(owner.getX() == (Game.screenWidth - 1*squareSize)) {
                        a = 7;
                    }
                    if(owner.getY() == 0) {
                        b = 0;
                    } else  if(owner.getY() == (Game.screenWidth - 7*squareSize)) {
                        b = 1;
                    } else if(owner.getY() == (Game.screenWidth - 6*squareSize)) {
                        b = 2;
                    } else  if(owner.getY() == (Game.screenWidth - 5*squareSize)) {
                        b = 3;
                    } else  if(owner.getY() == (Game.screenWidth - 4*squareSize)) {
                        b = 4;
                    } else  if(owner.getY() == (Game.screenWidth - 3*squareSize)) {
                        b = 5;
                    } else  if(owner.getY() == (Game.screenWidth - 2*squareSize)) {
                        b = 6;
                    } else  if(owner.getY() == (Game.screenWidth - 1*squareSize)) {
                        b = 7;
                    }

                    int x = 0;
                    int y = 0;
                    if(container.getX() == 0) {
                        x = 0;
                    } else  if(container.getX() == (Game.screenWidth - 7*squareSize)) {
                        x = 1;
                    } else if(container.getX() == (Game.screenWidth - 6*squareSize)) {
                        x = 2;
                    } else  if(container.getX() == (Game.screenWidth - 5*squareSize)) {
                        x = 3;
                    } else  if(container.getX() == (Game.screenWidth - 4*squareSize)) {
                        x = 4;
                    } else  if(container.getX() == (Game.screenWidth - 3*squareSize)) {
                        x = 5;
                    } else  if(container.getX() == (Game.screenWidth - 2*squareSize)) {
                        x = 6;
                    } else  if(container.getX() == (Game.screenWidth - 1*squareSize)) {
                        x = 7;
                    }
                    if(container.getY() == 0) {
                        y = 0;
                    } else  if(container.getY() == (Game.screenWidth - 7*squareSize)) {
                        y = 1;
                    } else if(container.getY() == (Game.screenWidth - 6*squareSize)) {
                        y = 2;
                    } else  if(container.getY() == (Game.screenWidth - 5*squareSize)) {
                        y = 3;
                    } else  if(container.getY() == (Game.screenWidth - 4*squareSize)) {
                        y = 4;
                    } else  if(container.getY() == (Game.screenWidth - 3*squareSize)) {
                        y = 5;
                    } else  if(container.getY() == (Game.screenWidth - 2*squareSize)) {
                        y = 6;
                    } else  if(container.getY() == (Game.screenWidth - 1*squareSize)) {
                        y = 7;
                    }

                    String move = ((char)(a + 97)) + "";
                    move = move.concat((8 - b) + " ");
                    move = move.concat((char)(x + 97) + "");
                    move = move.concat((8 - y) + "");

                    if(Game.drawOffer){
                        move = move.concat(" draw?" );
                    }

                    ReadInput r = new ReadInput();
                    r.callfromReplay = false;
                    r.setFlippy(flippy);

                    if(r.readInput(move)){
                        owner.removeView(view);
                        if(container.getChildAt(1) != null) {
                            container.removeView(container.getChildAt(1));
                        }
                        container.addView(view);
                        board.boardChecker();
                    }

                    ImageAdapter adapter = (ImageAdapter)chessboardGridView.getAdapter();
                    adapter.notifyDataSetChanged();
                }
              //  System.out.println("ACTION_DROP");
                break;
            case DragEvent.ACTION_DRAG_ENDED:
              //  System.out.println("ACTION_DRAG_ENDED");
                break;
        }
        return true;
    }
}
