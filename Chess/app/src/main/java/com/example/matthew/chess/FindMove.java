package com.example.matthew.chess;

import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.Random;

public class FindMove {
	
	public FindMove() {
		Board board = Game.getBoard();
		boolean currentPlayer = Game.getCurrentPlayer();
		ArrayList<Position> piecePositions = new ArrayList<Position>();
		
		for(int i = 0; i < board.positions.length; i++) {
            for(int j = 0; j < board.positions[i].length; j++) { 
            	if(board.getPosition(j, i).isOccupied()) {
	            	Piece piece = board.getPosition(j, i).getPiece();
	            	if(piece.getPlayer().getColor() == currentPlayer) {
	            		piecePositions.add(board.getPosition(j ,i));
	            	}
            	}
			} // inner for
		}// outer for
		Random rand = new Random();
		boolean validMove = false;
		int n;
		ArrayList<Position> validMoves = new ArrayList<Position>();;
		while(!validMove) {
			n = rand.nextInt(piecePositions.size());
			Position randPosition = piecePositions.get(n);
			piecePositions.remove(n);
			Piece randPiece = board.getPosition(randPosition.x, randPosition.y).getPiece();
			
			for(int i = 0; i < board.positions.length; i++) {
	            for(int j = 0; j < board.positions[i].length; j++) { 
	            	if (randPiece instanceof Pawn) {
						Pawn pawn = (Pawn)randPiece;
						pawn.setCallFromCheck(true);
						if(randPiece.isValid(board, randPosition.x, randPosition.y, j, i)) {
							validMoves.add(board.getPosition(j ,i));
						}
						pawn.setCallFromCheck(false);
					} else {
						if(randPiece.isValid(board, randPosition.x, randPosition.y, j, i)) {
							validMoves.add(board.getPosition(j ,i));
						}
					}
	            } // inner for
			}// outer for
			
			if(!validMoves.isEmpty()) {
				n = rand.nextInt(validMoves.size());
				Position finishPos = validMoves.get(n);
				finishPos.setPosition(randPosition.getPiece());
				randPosition.clearPosition();

				GridView gridView = (GridView) Game.flippy.findViewById(R.id.gridView);
				ViewGroup owner = (ViewGroup) gridView.getChildAt(randPosition.x  + (randPosition.y * 8));
				RelativeLayout container = (RelativeLayout) gridView.getChildAt(finishPos.x + (finishPos.y* 8));
				View view = owner.getChildAt(1);
				owner.removeView(view);
				if (container.getChildAt(1) != null) {
					container.removeView(container.getChildAt(1));
				}
				container.addView(view);
				board.boardChecker();

				validMove = true;
            	String move = ((char)(randPosition.x + 97)) + "";
            	move = move.concat((8 - randPosition.y) + " ");
            	move = move.concat((char)(finishPos.x + 97) + "");
            	move = move.concat((8 - finishPos.y) + "");
          	    Game.GRArray.getListOfMoves().add(move);
          	   // System.out.println(move);
				ReadInput.prevMove= move;
				ReadInput.undoLastTurn=false;
			}
			validMoves.clear();
		}// while loop
	}
}
