package com.example.matthew.chess;
import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ViewFlipper;

public class ImageAdapter extends BaseAdapter {
    private Context context;
    private ViewFlipper flippy;
    Activity mActivity;
    GridView chessboardGridView;
    Board board;
    ImageAdapter adapter;
    public ImageAdapter(Context c, ViewFlipper flippy, Activity mActivity, GridView chessboardGridView, Board board) {
        context = c;
        this.flippy = flippy;
        this.mActivity = mActivity;
        this.chessboardGridView = chessboardGridView;
        this.board = board;
        adapter = this;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        View squareContainerView = convertView;
        if ( convertView == null ) {
            //Inflate the layout
            final LayoutInflater layoutInflater = mActivity.getLayoutInflater();
            squareContainerView = layoutInflater.inflate(R.layout.square, null);
            if(!Board.callFromReplay) {
                PieceOnDragListener pL = new PieceOnDragListener(this.context);
                pL.setGrid(chessboardGridView);
                pL.setFlippy(flippy);
                pL.setBoard(board);
                squareContainerView.setOnDragListener(pL);
            }
            // Background
            ImageView squareView = (ImageView)squareContainerView.findViewById(R.id.square_background);
            squareView.setImageResource(this.aSquareImg[position]);

            //piece
            final ImageView pieceView = (ImageView) squareContainerView.findViewById(R.id.piece);

            if(board.getPosition((position%8), (position/8)).getPiece() != null) {
                String file = board.getPosition((position % 8), (position / 8)).getPiece().toString();
                if (file.equalsIgnoreCase("wp")) {
                    pieceView.setImageResource(R.drawable.wp);
                } else if (file.equalsIgnoreCase("wk")) {
                    pieceView.setImageResource(R.drawable.wk);
                } else if (file.equalsIgnoreCase("wr")) {
                    pieceView.setImageResource(R.drawable.wr);
                } else if (file.equalsIgnoreCase("wb")) {
                    pieceView.setImageResource(R.drawable.wb);
                } else if (file.equalsIgnoreCase("wn")) {
                    pieceView.setImageResource(R.drawable.wn);
                } else if (file.equalsIgnoreCase("wq")) {
                    pieceView.setImageResource(R.drawable.wq);
                } else if(file.equalsIgnoreCase("bp")) {
                    pieceView.setImageResource(R.drawable.bp);
                } else if (file.equalsIgnoreCase("bk")) {
                    pieceView.setImageResource(R.drawable.bk);
                } else if (file.equalsIgnoreCase("br")) {
                    pieceView.setImageResource(R.drawable.br);
                } else if (file.equalsIgnoreCase("bb")) {
                    pieceView.setImageResource(R.drawable.bb);
                } else if (file.equalsIgnoreCase("bn")) {
                    pieceView.setImageResource(R.drawable.bn);
                } else if (file.equalsIgnoreCase("bq")) {
                    pieceView.setImageResource(R.drawable.bq);
                }
                if(!Board.callFromReplay) {
                    pieceView.setTag(position);
                    pieceView.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            final ClipData data = ClipData.newPlainText("position", position + "");
                            final View.DragShadowBuilder pieceDragShadowBuilder = new View.DragShadowBuilder(pieceView);
                            v.startDrag(data, pieceDragShadowBuilder, v, 0);
                            return true;
                        }
                    });
                }
            }
        }
        return squareContainerView;
    }
    public ImageAdapter(Context c) {
        context = c;
    }

    public int getCount() {
        return aSquareImg.length;
    }

    public Object getItem(int position) {
        return board.getPosition((position % 8), (position / 8)).getPiece();
    }

    public long getItemId(int position) {
        return position;
    }

    static Integer[] aSquareImg = {
            R.drawable.lightsquare, R.drawable.darksquare, R.drawable.lightsquare, R.drawable.darksquare,
            R.drawable.lightsquare, R.drawable.darksquare, R.drawable.lightsquare, R.drawable.darksquare,
            R.drawable.darksquare, R.drawable.lightsquare, R.drawable.darksquare, R.drawable.lightsquare,
            R.drawable.darksquare, R.drawable.lightsquare, R.drawable.darksquare, R.drawable.lightsquare,
            R.drawable.lightsquare, R.drawable.darksquare, R.drawable.lightsquare, R.drawable.darksquare,
            R.drawable.lightsquare, R.drawable.darksquare, R.drawable.lightsquare, R.drawable.darksquare,
            R.drawable.darksquare, R.drawable.lightsquare, R.drawable.darksquare, R.drawable.lightsquare,
            R.drawable.darksquare, R.drawable.lightsquare, R.drawable.darksquare, R.drawable.lightsquare,
            R.drawable.lightsquare, R.drawable.darksquare, R.drawable.lightsquare, R.drawable.darksquare,
            R.drawable.lightsquare, R.drawable.darksquare, R.drawable.lightsquare, R.drawable.darksquare,
            R.drawable.darksquare, R.drawable.lightsquare, R.drawable.darksquare, R.drawable.lightsquare,
            R.drawable.darksquare, R.drawable.lightsquare, R.drawable.darksquare, R.drawable.lightsquare,
            R.drawable.lightsquare, R.drawable.darksquare, R.drawable.lightsquare, R.drawable.darksquare,
            R.drawable.lightsquare, R.drawable.darksquare, R.drawable.lightsquare, R.drawable.darksquare,
            R.drawable.darksquare, R.drawable.lightsquare, R.drawable.darksquare, R.drawable.lightsquare,
            R.drawable.darksquare, R.drawable.lightsquare, R.drawable.darksquare, R.drawable.lightsquare,
    };
}