package com.example.matthew.chess;
/** King class with King related methods such as validating movement, and toString
 * 
 * @author Matt Eder
 * @author Alex Weinrich
 */
public class King extends Piece {
    /** True if the King has moved at least once */
    boolean hasMoved = false;
    /** 
     * Only constructor for King, takes in a player field then super's the
     * piece constructor
     * @param player determines the owning player of the piece
     */
	public King(Player player) {
		super(player);
	}
	/** isValid to check whether the King is moving to a correct location,
    which includes:
    -One space in any direction
    -Not into check
    -Castling
    * 
    * @param board gives the current board to the king
    * @param fromX the original X position of the king
    * @param fromY the original Y position of the king
    * @param toX the X destination of the king
    * @param toY the Y destination of the king
    * @return returns a boolean for if its a valid move or not
    */
	public boolean isValid(Board board, int fromX, int fromY , int toX, int toY) {
        try {
        	if(!super.isValid(board, fromX, fromY, toX, toY)) {
        		return false;
            }
		} catch(ArrayIndexOutOfBoundsException e){
			return false;
		}
        
        if ((((fromX!=4) || (fromY!=7)) && (board.getPosition(fromX, fromY).getPiece().getPlayer().getColor())) || 
            (((fromX!=4) || (fromY!=0)) && (!board.getPosition(fromX, fromY).getPiece().getPlayer().getColor()))) {
            	hasMoved = true;
        }
        
		// moves only one space diagonally, vertically, horizontally  
		if( (fromX + 1 == toX && fromY + 1 == toY) ||  (fromX - 1 == toX && fromY - 1 == toY) ||
		    (fromX + 1 == toX && fromY - 1 == toY) ||  (fromX - 1 == toX && fromY + 1 == toY) ||
		    (fromX == toX && fromY + 1 == toY)     ||  (fromX == toX && fromY - 1 == toY)     ||
		    (fromX + 1 == toX && fromY == toY)     ||  (fromX - 1 == toX && fromY == toY) ) {
            	return true;
		}
                //Check if the king has moved
                else if (!hasMoved)
                {
                    //Check if its moving to the left two
                    if (toX==2 && toY==fromY)
                    {
                        //Check if the two nextdoor spots are open
                        if (!board.getPosition(3,toY).isOccupied() && !board.getPosition(2,toY).isOccupied() && !board.getPosition(1,toY).isOccupied() && board.getPosition(0,toY).isOccupied())
                        {
                            //if theres a rook in the left corner
                            if (board.getPosition(0,toY).getPiece() instanceof Rook)
                            {
                                //if the rook has not moved
                                if (!((Rook) board.getPosition(0,toY).getPiece()).getHasMoved())
                                {
                                    //if the king is in check
                                    Check c = new Check();
                                    int gameState;
                                    gameState = c.gameStateCheck(board);
                                    if (gameState == 0)
                                    {
                                        //If the king moves through check
                                        if (!c.inCheck(board, fromX, fromY, toX+1, toY))
                                        {
                                            board.getPosition(3, toY).setPosition(board.getPosition(0, toY).clearPosition());
                                            hasMoved=true;
                                            return true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //Check if its moving right two
                    else if(toX==6 && toY==fromY)
                    {
                        //Check if the two nextdoor spots are open
                        if (!board.getPosition(5,toY).isOccupied() && !board.getPosition(6,toY).isOccupied() && board.getPosition(7,toY).isOccupied())
                        {
                            //if theres a rook in the left corner
                            if (board.getPosition(7,toY).getPiece() instanceof Rook)
                            {
                                //if the rook has not moved
                                if (!((Rook) board.getPosition(7,toY).getPiece()).getHasMoved())
                                {
                                    //if the king is in check
                                    Check c = new Check();
                                    int gameState;
                                    gameState = c.gameStateCheck(board);
                                    if (gameState == 0)
                                    {
                                        //If the king moves through check
                                        if (!c.inCheck(board, fromX, fromY, toX-1, toY))
                                        {
                                            board.getPosition(5, toY).setPosition(board.getPosition(7, toY).clearPosition());
                                            hasMoved=true;
                                            return true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
		return false;
	}
	/** To string that returns K for the king class
     * 
     * @return returns the super toString plus K for king
     */
    public String toString() {
    	return super.toString() + "k";
    }
}
