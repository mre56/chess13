package com.example.matthew.chess;

import android.content.ClipData;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;
import android.widget.ImageView.ScaleType;
/**
 * This class takes the user input and decides if it is a valid move.
 *
 * @author Matthew Eder & Alex Weinrich
 */
public class ReadInput {
    /**
     * checked by game to see if input of valid
     */
    boolean valid = false;
    /**
     * keeps draw whether a draw was offered last turn
     */

    static ViewFlipper flippy = null;
    static boolean callfromReplay = false;
    static Board board;
    String input;
    static String prevMove = "";
    static Piece captured = null;
    static boolean undoLastTurn = false;
    public ReadInput() {
    }

    /**
     * Process the input given by the user to tell whether the input is in a valid format
     * then in if the move is a valid chess move. It will change the gameOver state in Game
     * class if a draw is accepted or player resigns
     *
     * @param input The move the user inputs
     */
    public boolean readInput(String input) {
        this.input = input;
        String pattern = "([a-h][1-8]\\s+[a-h][1-8](\\s+(draw\\?|N|Q|R|B))?|draw|resign|ai|undo)\\s*";
        TextView text;
        board = Game.getBoard();
        if (callfromReplay) {
            text = (TextView) flippy.findViewById(R.id.replayTextView);
        } else {
            text = (TextView) flippy.findViewById(R.id.textView);
        }
        if (!input.matches(pattern)) {
            text.setText("Illegal move, try again");
            return false;
        } else {
            String[] inputArray = input.split(" ");

            String init; // initial position or resign or draw
            init = inputArray[0];

            if (init.equals("resign")) {
                valid = true;
                Game.gameOver = true;
                if (Game.getCurrentPlayer())
                    text.setText("White resigns");
                else
                    text.setText("Black resigns");
                if (!callfromReplay) {
                    Game.GRArray.getListOfMoves().add(input);
                    board.endGamePrompt();
                }
                return true;
            }

            if (init.equals("draw")) {
                if (!Game.drawOffer) {
                    text.setText("Illegal move, try again");
                    return false;
                }
                valid = true;
                Game.draw = true;
                Game.gameOver = true;
                text.setText("Draw accepted");
                if (!callfromReplay) {
                    Game.GRArray.getListOfMoves().add(input);
                    board.endGamePrompt();
                }
                return true;
            }

            if (init.equalsIgnoreCase("ai")) {
                new FindMove();
                valid = true;
                return true;
            }

            if (init.equalsIgnoreCase("undo")) {
                if (!undoLastTurn && Game.turns != 0) {
                    String[] prevMoveArray = prevMove.split(" ");
                    final int fromY = 8 - Character.getNumericValue(prevMoveArray[0].charAt(1));
                    final int fromX = (int) prevMoveArray[0].charAt(0) - 97;  // ascii value of 'a' is 97
                    final int toY = 8 - Character.getNumericValue(prevMoveArray[1].charAt(1));
                    final int toX = (int) prevMoveArray[1].charAt(0) - 97;
                    Piece prevMovingPiece = board.getPosition(toX, toY).clearPosition();
                    board.getPosition(fromX, fromY).setPosition(prevMovingPiece);
                    if(captured != null) {
                        board.getPosition(toX, toY).setPosition(captured);
                    }

                    board.printBoard();
                    if (!callfromReplay) {
                        Game.GRArray.getListOfMoves().add("undo");
                    }
                    valid = true;
                    undoLastTurn = true;
                    board.boardChecker();
                    return true;
                } else {
                    if (Game.getCurrentPlayer())
                        text.setText("White's move");
                    else
                        text.setText("Black's move");
                    text.append(", can't undo");
                    return true;
                }
            }


            String end; //ending position
            end = inputArray[1];

            if (inputArray.length > 2) {
                String optionalArg = inputArray[2];
                if (optionalArg.equals("draw?"))
                    Game.drawOffer = true;
            } else {
                Game.drawOffer = false;
            }

            int fromX, fromY, toX, toY;
            fromY = 8 - Character.getNumericValue(init.charAt(1));
            fromX = (int) init.charAt(0) - 97;  // ascii value of 'a' is 97

            Position start = board.getPosition(fromX, fromY);

            if (start.getPiece() != null && start.getPiece().getPlayer().getColor() == Game.getCurrentPlayer()) {
                Piece piece = start.getPiece();
                toY = 8 - Character.getNumericValue(end.charAt(1));
                toX = (int) end.charAt(0) - 97;

                if (piece.isValid(board, fromX, fromY, toX, toY)) {
                    // Test for pawn promotion
                    if ((piece instanceof Pawn) && (((toY == 0) &&
                            (board.getPosition(fromX, fromY).getPiece().getPlayer().getColor())) ||
                            ((toY == 7) && (!board.getPosition(fromX, fromY).getPiece().getPlayer().getColor())))) {
                        try {
                            boolean pawnP = ((Pawn) piece).pawnPromotion(board, fromX, fromY, toX, toY, inputArray[2]);
                            if (pawnP) {
                                valid = true;
                                if (!callfromReplay) {
                                    Game.GRArray.getListOfMoves().add(input);
                                    undoLastTurn = false;
                                }
                                prevMove = input;
                                return true;
                            } else {
                                return false;
                            }
                        } catch (ArrayIndexOutOfBoundsException e) {
                            text.setText("Illegal move, try again");
                            return false;
                        }
                    } else {
                        Position finish = board.getPosition(toX, toY);
                        captured = finish.clearPosition();
                        finish.setPosition(piece);
                        start.clearPosition();
                        valid = true;
                        if (callfromReplay) {
                            setPieceImages(fromX, fromY, toX, toY);
                        } else {
                            Game.GRArray.getListOfMoves().add(input);
                            undoLastTurn = false;
                        }
                        prevMove = input;
                        return true;
                    }
                } else {
                    text.setText("Illegal move, try again");
                    return false;
                }

            } else {
                text.setText("Illegal move, try again");
                return false;
            }
        }
    }

    public void setFlippy(ViewFlipper flippy) {
        this.flippy = flippy;
    }

    public void setPieceImages(int fromX, int fromY, int toX, int toY) {
        GridView gridView = (GridView) flippy.findViewById(R.id.gridView2);
        ViewGroup owner = (ViewGroup) gridView.getChildAt(fromX + (fromY * 8));
        RelativeLayout container = (RelativeLayout) gridView.getChildAt(toX + (toY * 8));
        View view = owner.getChildAt(1);
        owner.removeView(view);
        if (container.getChildAt(1) != null) {
            container.removeView(container.getChildAt(1));
        }
        container.addView(view);
        board.boardChecker();
    }
}