package com.example.matthew.chess;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

class GameRecordArray implements Serializable {

	private static final long serialVersionUID = -7044049932301432791L;
	ArrayList<String> listOfMoves = new ArrayList<String>();;
	String title;
	Date date;
	public GameRecordArray() {
		title = "Untitled";
		Calendar cal = Calendar.getInstance();
		date = cal.getTime();
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	public void addMoves(String moves) {
		listOfMoves.add(moves);
	}

	public ArrayList<String> getListOfMoves() {
		return listOfMoves;
	}
}