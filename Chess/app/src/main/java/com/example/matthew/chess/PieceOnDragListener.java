package com.example.matthew.chess;

import android.content.Context;
import android.view.DragEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;

import android.widget.RelativeLayout;
import android.widget.ViewFlipper;

public class PieceOnDragListener extends Thread implements View.OnDragListener  {
    private Context context;
    private boolean insideOfMe = false;
    GridView chessboardGridView = null;
    ViewFlipper flippy = null;
    Board board;
    public PieceOnDragListener(Context pContext) {
        this.context = pContext;
    }
    public void setGrid(GridView chessboardGridView) {
        this.chessboardGridView = chessboardGridView;
    }
    public void setFlippy(ViewFlipper flippy) {
        this.flippy = flippy;
    }
    public void setBoard(Board board) {
        this.board = board;
    }
    public boolean onDrag(View pV, DragEvent pEvent) {
        final int lEventAction = pEvent.getAction();

        switch(lEventAction) {

            case DragEvent.ACTION_DRAG_STARTED:
                //System.out.println("ACTION_DRAG_STARTED");
                break;
            case DragEvent.ACTION_DRAG_ENTERED:
                insideOfMe = true;
               // System.out.println("ACTION_DRAG_ENTERED");
                break;
            case DragEvent.ACTION_DRAG_EXITED:
                insideOfMe = false;
             //   System.out.println("ACTION_DRAG_EXITED");
                break;
            case DragEvent.ACTION_DROP:
                if (insideOfMe){
                    View view = (View) pEvent.getLocalState();
                    ViewGroup owner = (ViewGroup) view.getParent();
                    RelativeLayout container = (RelativeLayout) pV;

                    int a = 0;
                    int b = 0;
                    if(owner.getX() == 0) {
                        a = 0;
                    } else  if(owner.getX() == 40) {
                        a = 1;
                    } else if(owner.getX() == 80) {
                        a = 2;
                    } else  if(owner.getX() == 120) {
                        a = 3;
                    } else  if(owner.getX() == 160) {
                        a = 4;
                    } else  if(owner.getX() == 200) {
                        a = 5;
                    } else  if(owner.getX() == 240) {
                        a = 6;
                    } else  if(owner.getX() == 280) {
                        a = 7;
                    }
                    if(owner.getY() == 3) {
                        b = 0;
                    } else  if(owner.getY() == 38) {
                        b = 1;
                    } else if(owner.getY() == 73) {
                        b = 2;
                    } else  if(owner.getY() == 108) {
                        b = 3;
                    } else  if(owner.getY() == 143) {
                        b = 4;
                    } else  if(owner.getY() == 178) {
                        b = 5;
                    } else  if(owner.getY() == 213) {
                        b = 6;
                    } else  if(owner.getY() == 248) {
                        b = 7;
                    }

                    int x = 0;
                    int y = 0;
                    if(container.getX() == 0) {
                        x = 0;
                    } else  if(container.getX() == 40) {
                        x = 1;
                    } else if(container.getX() == 80) {
                        x = 2;
                    } else  if(container.getX() == 120) {
                        x = 3;
                    } else  if(container.getX() == 160) {
                        x = 4;
                    } else  if(container.getX() == 200) {
                        x = 5;
                    } else  if(container.getX() == 240) {
                        x = 6;
                    } else  if(container.getX() == 280) {
                        x = 7;
                    }
                    if(container.getY() == 3) {
                        y = 0;
                    } else  if(container.getY() == 38) {
                        y = 1;
                    } else if(container.getY() == 73) {
                        y = 2;
                    } else  if(container.getY() == 108) {
                        y = 3;
                    } else  if(container.getY() == 143) {
                        y = 4;
                    } else  if(container.getY() == 178) {
                        y = 5;
                    } else  if(container.getY() == 213) {
                        y = 6;
                    } else  if(container.getY() == 248) {
                        y = 7;
                    }

                    String move = ((char)(a + 97)) + "";
                    move = move.concat((8 - b) + " ");
                    move = move.concat((char)(x + 97) + "");
                    move = move.concat((8 - y) + "");
                    if(Game.drawOffer){
                        move = move.concat(" draw?" );
                    }
                    ReadInput r = new ReadInput();
                    r.callfromReplay = false;
                    r.setFlippy(flippy);
                    if(r.readInput(move)){
                        owner.removeView(view);
                        if(container.getChildAt(1) != null) {
                            container.removeView(container.getChildAt(1));
                        }
                        container.addView(view);
                        board.boardChecker();
                    }

                    ImageAdapter adapter = (ImageAdapter)chessboardGridView.getAdapter();
                    adapter.notifyDataSetChanged();
                }
              //  System.out.println("ACTION_DROP");
                break;
            case DragEvent.ACTION_DRAG_ENDED:
              //  System.out.println("ACTION_DRAG_ENDED");
                break;
        }
        return true;
    }
}
