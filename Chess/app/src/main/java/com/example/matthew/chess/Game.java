package com.example.matthew.chess;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.RequiresPermission;
import android.support.v7.view.menu.MenuBuilder;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import android.widget.ViewFlipper;
import com.google.android.gms.common.api.GoogleApiClient;
import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Scanner;
import java.io.OutputStreamWriter;

/**
 * This class instantiates core classes, variables and manages the general
 * flow of the game of chess
 *
 * @author Matthew Eder & Alex Weinrich
 */
public class Game implements Serializable {

    private static final long serialVersionUID = -5090542815754459925L;
    /**
     * Scanner to read user input
     */
    Scanner reader;
    /**
     * tells whether the game has ended in a draw
     */
    static boolean draw = false;
    /**
     * tells whether the game has ended in a stalemate
     */
    static boolean stalemate = false;
    /**
     * tells whether the game has ended
     */
    static boolean gameOver = false;
    /**
     * @see Board
     */
    static Board board;
    /**
     * White is sent param true
     *
     * @see Player
     */
    static Player white;
    /**
     * Black is sent param false
     *
     * @see Player
     */
    static Player black;
    /**
     * keeps track of the total number of turns taken
     */
    static int turns = 0;
    /**
     * Output stream from write to games.ser file
     */
    private ObjectOutputStream obj_out;
    /**
     * Object the holds list of games
     */
    public static ArrayList<GameRecordArray> gameList = new ArrayList<>();
    /**
     * Object that has list of moves, title and date
     */
    static GameRecordArray GRArray;
    /**
     * Input stream from to read from games.ser file
     */
    private ObjectInputStream obj_in;
    /**
     * Input from user
     */
    private String input = "";
    private ReadInput in;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    File games = new File("games.ser");
    protected static Context context;
    protected static Activity mActivity;
    protected static ViewFlipper flippy;
    protected Toolbar toolbar;
    static boolean drawOffer = false;

    /**
     * This the constructor of the Game class.
     * Instantiates two Players, Board, ReadInput, Scanner.
     * A loop ask for input and keeping track of the turns
     * until the game is over and then prints out draw or
     * who won.
     *
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public Game(Activity mActivity, Context context, ViewFlipper flippy, Toolbar toolbar) throws IOException, ClassNotFoundException {
        white = new Player(true); // lightsquare == true
        black = new Player(false); // blacksquare == false
        this.mActivity = mActivity;
        this.flippy = flippy;
        this.context = context;
        this.toolbar = toolbar;
        flippy.setDisplayedChild(0);

        try {
            //Read from disk using FileInputStream
            System.out.println("in 1 " + context.getFilesDir());
            FileInputStream f_in = context.openFileInput("games.ser");
            obj_in = new ObjectInputStream(f_in);
            //Read an object
            Object obj = obj_in.readObject();
            if (obj instanceof ArrayList) {
                gameList = (ArrayList<GameRecordArray>) obj;
            }
            f_in.close();
            obj_in.close();
        } catch (Exception e) {  // EOFExpection && filenotfound
            System.out.println(e);
            FileOutputStream fOut = context.openFileOutput("games.ser", context.MODE_PRIVATE);
            OutputStreamWriter osw = new OutputStreamWriter(fOut);
            fOut.close();
            osw.close();
        }

        final Button newGameButton = (Button) flippy.findViewById(R.id.newGameButton);
        final Button replayButton = (Button) flippy.findViewById(R.id.replayButton);
        assert newGameButton != null;
        newGameButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    twoPlayerGame();
                } catch (IOException e) {
                }
            }
        });

        assert replayButton != null;
        replayButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    replayGame();
                } catch (IOException | ClassNotFoundException e) {
                }

            }
        });
    }

    private void twoPlayerGame() throws IOException {
        turns = 0;
        GRArray = new GameRecordArray();
        board = new Board(white, black, mActivity, context, flippy, toolbar);
        board.callFromReplay = false;
        flippy.setDisplayedChild(1);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flippy.setDisplayedChild(0);
                toolbar.setNavigationIcon(null);
                return;
            }
        });
        final Button AIButton = (Button) flippy.findViewById(R.id.AIButton);
        final Button resignButton = (Button) flippy.findViewById(R.id.resignButton);
        final Button undoButton = (Button) flippy.findViewById(R.id.undoButton);
        final Button drawButton = (Button) flippy.findViewById(R.id.drawButton);
        final TextView textDisplay = (TextView) flippy.findViewById(R.id.textView);
        textDisplay.setKeyListener(null);
        AIButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                new FindMove();
            }
        });
        resignButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ReadInput r = new ReadInput();
                r.setFlippy(flippy);
                r.readInput("resign");
            }
        });
        undoButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ReadInput r = new ReadInput();
                r.setFlippy(flippy);
                r.readInput("undo");
            }
        });
        drawButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(drawOffer) {
                    ReadInput r = new ReadInput();
                    r.setFlippy(flippy);
                    r.readInput("draw");
                } else {
                    drawOffer = true;
                    textDisplay.append(" draw will be offered");
                }
            }
        });
        board.printBoard();
    }

    private void replayGame() throws IOException, ClassNotFoundException {
        turns = 0;
        flippy.setDisplayedChild(2);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flippy.setDisplayedChild(0);
                toolbar.setNavigationIcon(null);
                toolbar.getMenu().clear();
                return;
            }
        });

        final ListView lv = (ListView) flippy.findViewById(R.id.listView);
        try {
            //Read from disk using FileInputStream
            FileInputStream f_in = context.openFileInput("games.ser");
            obj_in = new ObjectInputStream(f_in);
            //Read an object
            Object obj = obj_in.readObject();
            if (obj instanceof ArrayList) {
                gameList = (ArrayList<GameRecordArray>) obj;
            }
            f_in.close();
            obj_in.close();
        } catch (Exception e) {
            System.out.println(e);
        }

        if (gameList == null || gameList.isEmpty()) {
            ArrayList<String> empArray = new ArrayList<String>();
            empArray.add("No save games");
            lv.setAdapter(new ArrayAdapter<String>(mActivity, R.layout.listitem, empArray));
            return;
        }

        final ArrayList<String> gameTitleList = new ArrayList<>();
        for (GameRecordArray GRA : gameList) {
            gameTitleList.add(GRA.getTitle() + "\n" + GRA.date);
        }
        //set list view to game titles
        final ListAdapter adapter = new ArrayAdapter<String>(mActivity, R.layout.listitem, gameTitleList);
        lv.setAdapter(adapter);
        lv.setSelection(0);

        Menu menu = toolbar.getMenu();
        MenuInflater inflater = mActivity.getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        final MenuItem menuItem = menu.findItem(R.id.sort);

        menuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener(){
            public boolean onMenuItemClick(MenuItem item){
               // gameTitleList = new ArrayList<String>(Arrays.asList(gameTitleList));
                Collections.sort(gameTitleList);

                ((BaseAdapter)adapter).notifyDataSetChanged();
                return true;
            }
        });
        lv.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView,
                                            View view, int i, long l) {
                        flippy.setDisplayedChild(3);
                        toolbar.getMenu().clear();
                        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
                        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                flippy.setDisplayedChild(0);
                                toolbar.setNavigationIcon(null);
                                return;
                            }
                        });

                        final Button nextButton = (Button) flippy.findViewById(R.id.nextButton);
                        nextButton.setEnabled(true);
                        String s = (String)lv.getItemAtPosition(i);
                        String sa[] = s.split("\n");
                        ArrayList<String> replayMovestemp = null;
                        for(GameRecordArray GRA: gameList) {
                            if(GRA.getTitle().equalsIgnoreCase(sa[0])){
                                replayMovestemp = GRA.getListOfMoves();
                                break;
                            }
                        }
                        final ArrayList<String> replayMoves = replayMovestemp;

                        board = new Board(white, black, mActivity, context, flippy, toolbar);
                        board.callFromReplay = true;
                        board.printBoard();
                        //board.callFromReplay = false;
                        nextButton.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                String move = replayMoves.remove(0);
                                ReadInput readIn = new ReadInput();
                                readIn.callfromReplay = true;
                                readIn.undoLastTurn = false;
                                readIn.setFlippy(flippy);
                                readIn.readInput(move);
                                if(replayMoves.size() == 0){
                                    nextButton.setEnabled(false);
                                }
                            }
                        });

                    }
                }
        );
    }

    /**
     * @return board object of the game
     */
    public static Board getBoard() {
        return board;
    }

    /**
     * @return lightsquare player of the game
     */
    public static Player getPlayerW() {
        return white;
    }

    /**
     * @return blacksquare player of the game
     */
    public static Player getPlayerB() {
        return black;
    }

    /**
     * @return number of turns taken
     */
    public static int getTurns() {
        return turns;
    }

    /**
     * @return current player of the game as a boolean, true for lightsquare, false for blacksquare
     */
    public static boolean getCurrentPlayer() {
        // White
// blacksquare
        return Game.getTurns() % 2 == 0;
    }
}


