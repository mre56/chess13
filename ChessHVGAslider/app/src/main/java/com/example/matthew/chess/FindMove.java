package com.example.matthew.chess;

import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.Random;

public class FindMove {
	
	public String FindMove() {
		Board board = Game.getBoard();
		boolean currentPlayer = Game.getCurrentPlayer();
		ArrayList<Position> piecePositions = new ArrayList<Position>();
		
		for(int i = 0; i < board.positions.length; i++) {
            for(int j = 0; j < board.positions[i].length; j++) { 
            	if(board.getPosition(j, i).isOccupied()) {
	            	Piece piece = board.getPosition(j, i).getPiece();
	            	if(piece.getPlayer().getColor() == currentPlayer) {
	            		piecePositions.add(board.getPosition(j ,i));
	            	}
            	}
			} // inner for
		}// outer for
		Random rand = new Random();
		boolean validMove = false;
		int n;

		ArrayList<Position> validMoves = new ArrayList<Position>();;
		while(!validMove) {
			n = rand.nextInt(piecePositions.size());
			Position startPosition = piecePositions.get(n);
			piecePositions.remove(n);
			Piece randPiece = board.getPosition(startPosition.x, startPosition.y).getPiece();
			
			for(int i = 0; i < board.positions.length; i++) {
	            for(int j = 0; j < board.positions[i].length; j++) { 
	            	if (randPiece instanceof Pawn) {
						Pawn pawn = (Pawn)randPiece;
						pawn.setCallFromCheck(true);
						if(randPiece.isValid(board, startPosition.x, startPosition.y, j, i)) {
							validMoves.add(board.getPosition(j ,i));
						}
						pawn.setCallFromCheck(false);
					} else {
						if(randPiece.isValid(board, startPosition.x, startPosition.y, j, i)) {
							validMoves.add(board.getPosition(j ,i));
						}
					}
	            } // inner for
			}// outer for
			
			if(!validMoves.isEmpty()) {
				Position finishPos = null;

					n = rand.nextInt(validMoves.size());
					finishPos = validMoves.get(n);


				//ReadInput.captured = board.getPosition(finishPos.x, finishPos.y).getPiece();
				String promo = "";
				boolean notPromote = true;
				if(randPiece instanceof Pawn){
					if(randPiece.getPlayer().getColor() ){
						if(finishPos.y == 0){
							promo = " Q";
							//Piece promoPiece = new Piece(board.getPosition(startPosition.x, startPosition.y).getPiece().getPlayer());
							//board.getPosition(startPosition.x, startPosition.y).clearPosition();
							//board.getPosition(finishPos.x, finishPos.y).setPosition(promoPiece);
							//notPromote = false;
							//ReadInput.prevMoveWasPromote = true;
						}
					} else {
						if(finishPos.y == 7){
							promo = " Q";
							//Piece promoPiece = new Piece(board.getPosition(startPosition.x, startPosition.y).getPiece().getPlayer());
							//board.getPosition(startPosition.x, startPosition.y).clearPosition();
							//board.getPosition(finishPos.x, finishPos.y).setPosition(promoPiece);
							//notPromote = false;
							//ReadInput.prevMoveWasPromote = true;
						}
					}
				}
				if(notPromote){
					//finishPos.setPosition(startPosition.getPiece());
					//startPosition.clearPosition();
					ReadInput.prevMoveWasPromote = false;
				}
				String move = ((char)(startPosition.x + 97)) + "";
				move = move.concat((8 - startPosition.y) + " ");
				move = move.concat((char)(finishPos.x + 97) + "");
				move = move.concat((8 - finishPos.y) + "");
				//ReadInput r = new ReadInput();
				//r.setFlippy(Game.flippy);
				//r.readInput(move+promo);

				return move+promo;
				/*GridView gridView = (GridView) Game.flippy.findViewById(R.id.gridView);
				ViewGroup owner = (ViewGroup) gridView.getChildAt(startPosition.x  + (startPosition.y * 8));
				RelativeLayout container = (RelativeLayout) gridView.getChildAt(finishPos.x + (finishPos.y* 8));
				View view = owner.getChildAt(1);
				owner.removeView(view);
				if (container.getChildAt(1) != null) {
					container.removeView(container.getChildAt(1));
				}


				validMove = true;
            	String move = ((char)(startPosition.x + 97)) + "";
            	move = move.concat((8 - startPosition.y) + " ");
            	move = move.concat((char)(finishPos.x + 97) + "");
            	move = move.concat((8 - finishPos.y) + "");
          	    Game.GRArray.getListOfMoves().add(move + promo);
				container.addView(view);
				ReadInput.prevMove = move;
				ReadInput.undoLastTurn = false;
				board.boardChecker();*/
			}
			validMoves.clear();
		}// while loop
		return "";
	}
}
