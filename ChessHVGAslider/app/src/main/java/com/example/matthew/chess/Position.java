package com.example.matthew.chess;
/**
 * This class represents a single position on the Board object
 * @author Matthew Eder & Alex Weinrich
 */
public class Position {
	/**
	 * the x position corresponds to the letter input 
	 */
    int x;
    /**
     * the y position corresponds to the number input
     */
    int y;
    /**
     * The piece object that occupies that (x,y) position on the board
     */
    Piece piece;
    /**
     * The constructor of the Position class
     * The piece is set to null 
     * @param x the x position corresponds to the letter input 
     * @param y the y position corresponds to the number input
     */
    public Position(int x, int y) {
        this.x = x;
        this.y = y;
        piece = null;
    }
    /**
     * @return Piece object
     */
    public Piece getPiece() {
    	return piece;
    }
    /**
     * Sets the piece field to the param given
     * @param piece object
     */
    public void setPosition(Piece piece) {
        this.piece = piece;
    }
    /**
     * @return true if the positions piece field isn't null
     */
    public boolean isOccupied() {
        if(piece != null)
            return true;
        return false;
    }
    /**
     * Sets the position piece field to null
     * @return piece that occupies the position
     */
    public Piece clearPosition() {
        Piece releasedPiece = this.piece;
        this.piece = null;
        return releasedPiece;
    }
    /**
     * For debugging purposes 
     * @return (x, y) the x and y coordinates formatted 
     */
    public String toString() {
		 return " (" + x + ", " + y + ") ";
	}
}
