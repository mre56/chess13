package com.example.matthew.chess;

import java.util.ArrayList;

public class Check {
	public Check() {
	}
	
	public boolean inCheck(Board board, int fromX, int fromY, int toX, int toY) { // if king is moving change things
		boolean currentPlayer = Game.getCurrentPlayer();
        boolean kingInCheck = false;
		boolean kingIsSet = false;
		Position king = null;
		Piece capturedPiece = null;
		Piece movingPiece = board.getPosition(fromX, fromY).clearPosition();
		
		if(board.getPosition(toX, toY).isOccupied()) {
			capturedPiece = board.getPosition(toX, toY).clearPosition(); // should not be own piece or same spot, checked in piece class
		}
		
		if(movingPiece instanceof King && movingPiece.getPlayer().getColor() == currentPlayer) {
			board.getPosition(toX, toY).setPosition(movingPiece);
			king = board.getPosition(toX, toY);
			kingIsSet = true;
		}
		
		if(!kingIsSet)
			board.getPosition(toX, toY).setPosition(movingPiece);
		
		for(int i = 0; i < board.positions.length; i++) {
            for(int j = 0; j < board.positions[i].length; j++) { 
            	if(board.getPosition(j, i).isOccupied()) {
	            	Piece piece = board.getPosition(j, i).getPiece();
	            	if (!kingIsSet) {
						if((piece instanceof King) && (piece.getPlayer().getColor() == currentPlayer)) { 
								king = board.getPosition(j, i);
								kingIsSet = true;
								i = 0;
								j = 0;
						} 
	            	}
					if(kingIsSet && piece.getPlayer().getColor() != currentPlayer) {
						if (piece instanceof Pawn) {
							Pawn pawn = (Pawn)piece;
							pawn.setCallFromCheck(true);
							kingInCheck = pawn.isValid(board, j, i, king.x, king.y);
							pawn.setCallFromCheck(false);
						} else {
							kingInCheck = piece.isValid(board, j, i, king.x, king.y);
						}
						if(kingInCheck) {
							board.getPosition(fromX, fromY).setPosition(movingPiece);
							if(capturedPiece != null){
								board.getPosition(toX, toY).setPosition(capturedPiece);
							} else {
								board.getPosition(toX, toY).clearPosition();
							}
							return true;
						}
					}
            	}
			} // end of inner for-each loop
		} // end of outer for-each loop	
		// reset original starting positions
		// still have other checks in isValid Piece subclasses to check
		board.getPosition(fromX, fromY).setPosition(movingPiece);
		if(capturedPiece != null) {
			board.getPosition(toX, toY).setPosition(capturedPiece);
		} else {
			board.getPosition(toX, toY).clearPosition();
		}
		if(kingInCheck) 
			return true;
		else
			return false;
	}
	
	// Check Game state
	// 0 = not in check, 1 in check, 2 Checkmate, 3 Stalemate
	public int gameStateCheck(Board board) {
		boolean currentPlayer = Game.getCurrentPlayer();
		boolean kingInCheck = false;
		boolean kingIsSet = false;
		boolean notCheckmate = false;
		boolean notStalemate = true;
		boolean stalemate = true;
		boolean check = false;
		Position king = null;
		Piece piece;
		Position checkingPiece; // piece causing check
		Piece removingCheckPiece; 
		
		for(int i = 0; i < board.positions.length; i++) {
            for(int j = 0; j < board.positions[i].length; j++) { 
            	if(board.getPosition(j, i).isOccupied() && !kingIsSet) {
	            	piece = board.getPosition(j, i).getPiece();
					if(piece instanceof King) {
						if(piece.getPlayer().getColor() == currentPlayer && !kingIsSet) { 
							king = board.getPosition(j, i);
							kingIsSet = true;
							i = 0;
							j = 0;
						}
					} 
            	}
            	
				if(board.getPosition(j, i).isOccupied() && kingIsSet) {
	            	piece = board.getPosition(j, i).getPiece();
	            	if (piece instanceof Pawn) {
						Pawn pawn = (Pawn)piece;
						pawn.setCallFromCheck(true);
						kingInCheck = pawn.isValid(board, j, i, king.x, king.y);
						pawn.setCallFromCheck(false);
					} else {
						kingInCheck = piece.isValid(board, j, i, king.x, king.y);
					}
					// check for mate
					if(kingInCheck) { // checkmate condition
						// king has valid move
						if( king.getPiece().isValid(board, king.x, king.y, king.x+1, king.y) ||
						    king.getPiece().isValid(board, king.x, king.y, king.x-1, king.y) || 
						    king.getPiece().isValid(board, king.x, king.y, king.x, king.y+1) || 
						    king.getPiece().isValid(board, king.x, king.y, king.x, king.y-1) || 
					        king.getPiece().isValid(board, king.x, king.y, king.x+1, king.y+1) || 
						    king.getPiece().isValid(board, king.x, king.y, king.x-1, king.y-1) || 
						    king.getPiece().isValid(board, king.x, king.y, king.x+1, king.y-1) ||
						    king.getPiece().isValid(board, king.x, king.y, king.x-1, king.y+1) ) { 
							return 1; //  in Check
						}

						check = true;
						checkingPiece = board.getPosition(j ,i);

						for(int m = 0; m < board.positions.length; m++) {
							for(int n = 0; n < board.positions[i].length; n++) { 
					            if(board.getPosition(n, m).isOccupied()) {
						            removingCheckPiece = board.getPosition(n, m).getPiece();
						            
						            if(removingCheckPiece.getPlayer().getColor() == currentPlayer) {
						            	notCheckmate = removingCheckPiece.isValid(board, n, m, checkingPiece.x, checkingPiece.y);
										if(notCheckmate) break;
										if(checkingPiece.getPiece() instanceof Rook) {
											Rook rook = (Rook)checkingPiece.getPiece();
											rook.isValid(board, j, i, king.x, king.y);
											ArrayList<Position> path = rook.getPath();
											ArrayList<Position> pathClone = (ArrayList<Position>)path.clone();
											for(Position p: pathClone) {
												if(!(removingCheckPiece instanceof King))
													notCheckmate = removingCheckPiece.isValid(board, n, m, p.x, p.y);
												if(notCheckmate) break;
											}
										 } else if(checkingPiece.getPiece() instanceof Bishop) {
											 Bishop bishop = (Bishop)checkingPiece.getPiece();
											 bishop.isValid(board, j, i, king.x, king.y);
											 ArrayList<Position> path = bishop.getPath();
											 ArrayList<Position> pathClone = (ArrayList<Position>)path.clone();
											 for(Position p: pathClone) {
												 if(!(removingCheckPiece instanceof King))
													 notCheckmate = removingCheckPiece.isValid(board, n, m, p.x, p.y);
												 if(notCheckmate) break;
											 }
										 } else if(checkingPiece.getPiece() instanceof Queen) {
											 Queen queen = (Queen)checkingPiece.getPiece();
											 queen.isValid(board, j, i, king.x, king.y);
											 ArrayList<Position> path = queen.getPath();
											 ArrayList<Position> pathClone = (ArrayList<Position>)path.clone();
											 for(Position p: pathClone) {
												 if(!(removingCheckPiece instanceof King))
													 notCheckmate = removingCheckPiece.isValid(board, n, m, p.x, p.y);
												 if(notCheckmate) break;
											 }
										 } 
						            }
					            }
								if(notCheckmate) break;
							} // end of inner for-each loop mate
							if(notCheckmate) break;
						} // end of outer for-each loop mate
						if(!notCheckmate) {
							return 2; // checkmate
						} else  {
							return 1; // in check
						}
					} else if(stalemate) { // king not in check stalemate condition
						// king has valid move
						if( king.getPiece().isValid(board, king.x, king.y, king.x+1, king.y) ||
							king.getPiece().isValid(board, king.x, king.y, king.x-1, king.y) || 
						    king.getPiece().isValid(board, king.x, king.y, king.x, king.y+1) || 
						    king.getPiece().isValid(board, king.x, king.y, king.x, king.y-1) || 
					        king.getPiece().isValid(board, king.x, king.y, king.x+1, king.y+1) || 
						    king.getPiece().isValid(board, king.x, king.y, king.x-1, king.y-1) || 
						    king.getPiece().isValid(board, king.x, king.y, king.x+1, king.y-1) ||
						    king.getPiece().isValid(board, king.x, king.y, king.x-1, king.y+1)) { 
							stalemate = false;
							break; 
						}
						// check if other pieces have valid moves
						if(piece.getPlayer().getColor() == currentPlayer){
							for(int m = 0; m < board.positions.length; m++) {
								for(int n = 0; n < board.positions[i].length; n++) { 
									if (piece instanceof Pawn) {
										Pawn pawn = (Pawn)piece;
										pawn.setCallFromCheck(true);
										notStalemate = pawn.isValid(board, j, i, n, m);
										pawn.setCallFromCheck(false);
									} else {
										notStalemate = piece.isValid(board, j, i, n, m);
									}
					            	if(notStalemate) { 
					            		stalemate = false;
					            		break;	
					            	}
								}
								if(!stalemate) break;
							}
						}	
					}
            	} // King is set board occupied if
			} // end of inner for-each loop
		} // end of outer for-each loop	
		
		if(check) {
			return 1; // in check
		} else if(stalemate) { 
			return 3; // Stalemate
		} else {
			return 0; // not in check
		}
	}
}