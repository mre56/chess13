package com.example.matthew.chess;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.RequiresPermission;
import android.support.v7.view.menu.MenuBuilder;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.google.android.gms.common.api.GoogleApiClient;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Scanner;
import java.io.OutputStreamWriter;

/**
 * This class instantiates core classes, variables and manages the general
 * flow of the game of chess
 *
 * @author Matthew Eder & Alex Weinrich
 */
public class Game implements Serializable {

    private static final long serialVersionUID = -5090542815754459925L;
    /**
     * Scanner to read user input
     */
    Scanner reader;
    /**
     * tells whether the game has ended in a draw
     */
    static boolean draw = false;
    /**
     * tells whether the game has ended in a stalemate
     */
    static boolean stalemate = false;
    /**
     * tells whether the game has ended
     */
    static boolean gameOver = false;
    /**
     * @see Board
     */
    static Board board;
    /**
     * White is sent param true
     *
     * @see Player
     */
    static Player white;
    /**
     * Black is sent param false
     *
     * @see Player
     */
    static Player black;
    /**
     * keeps track of the total number of turns taken
     */
    static int turns = 0;
    /**
     * Output stream from write to games.ser file
     */
    private ObjectOutputStream obj_out;
    /**
     * Object the holds list of games
     */
    public static ArrayList<GameRecordArray> gameList = new ArrayList<>();
    /**
     * Object that has list of moves, title and date
     */
    static GameRecordArray GRArray;
    /**
     * Input stream from to read from games.ser file
     */
    private ObjectInputStream obj_in;
    /**
     * Input from user
     */
    private String input = "";
    private ReadInput in;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    File games = new File("games.ser");
    protected static Context context;
    protected static Activity mActivity;
    protected static ViewFlipper flippy;
    protected Toolbar toolbar;
    static boolean drawOffer = false;
    static int screenWidth;
    static boolean abcClick = false;
    static boolean dateClick = false;
    static boolean drawWasPressed = false;
    static int turnDrawWasPressed = 0;
    /**
     * This the constructor of the Game class.
     * Instantiates two Players, Board, ReadInput, Scanner.
     * A loop ask for input and keeping track of the turns
     * until the game is over and then prints out draw or
     * who won.
     *
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public Game(Activity mActivity, Context context, ViewFlipper flippy, Toolbar toolbar) throws IOException, ClassNotFoundException {
        white = new Player(true); // lightsquare == true
        black = new Player(false); // blacksquare == false
        this.mActivity = mActivity;
        this.flippy = flippy;
        this.context = context;
        this.toolbar = toolbar;
        flippy.setDisplayedChild(0);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        screenWidth = displaymetrics.widthPixels;

        try {
            //Read from disk using FileInputStream
            FileInputStream f_in = context.openFileInput("games.ser");
            obj_in = new ObjectInputStream(f_in);
            //Read an object
            Object obj = obj_in.readObject();
            if (obj instanceof ArrayList) {
                gameList = (ArrayList<GameRecordArray>) obj;
            }
            f_in.close();
            obj_in.close();
        } catch (Exception e) {  // EOFExpection && filenotfound
            System.out.println(e);
            FileOutputStream fOut = context.openFileOutput("games.ser", context.MODE_PRIVATE);
            OutputStreamWriter osw = new OutputStreamWriter(fOut);
            fOut.close();
            osw.close();
        }

        final Button newGameButton = (Button) flippy.findViewById(R.id.newGameButton);
        final Button replayButton = (Button) flippy.findViewById(R.id.replayButton);
        assert newGameButton != null;
        newGameButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    twoPlayerGame();
                } catch (IOException e) {
                }
            }
        });

        assert replayButton != null;
        replayButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    replayGame();
                } catch (IOException | ClassNotFoundException e) {
                }

            }
        });
    }

    private void twoPlayerGame() throws IOException {
        turns = 0;
        drawWasPressed = false;
        turnDrawWasPressed = 0;
        drawOffer = false;
        GRArray = new GameRecordArray();
        board = new Board(white, black, mActivity, context, flippy, toolbar);
        board.callFromReplay = false;
        flippy.setDisplayedChild(1);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flippy.setDisplayedChild(0);
                toolbar.setNavigationIcon(null);
                return;
            }
        });
        final Button AIButton = (Button) flippy.findViewById(R.id.AIButton);
        final Button resignButton = (Button) flippy.findViewById(R.id.resignButton);
        final Button undoButton = (Button) flippy.findViewById(R.id.undoButton);
        final Button drawButton = (Button) flippy.findViewById(R.id.drawButton);
        final TextView textDisplay = (TextView) flippy.findViewById(R.id.textView);
        AIButton.setWidth(screenWidth/4);
        resignButton.setWidth(screenWidth/4);
        undoButton.setWidth(screenWidth/4);
        drawButton.setWidth(screenWidth/4);
        textDisplay.setKeyListener(null);
        textDisplay.setText("White's Move");

        AIButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                drawOffer = false;
                FindMove f = new FindMove();
                String s = f.FindMove();
                ReadInput r = new ReadInput();
                r.callfromReplay = false;
                r.setFlippy(flippy);
                System.out.println(s + " " +turns);
                r.readInput(s);
                board.printBoard();
                board.boardChecker();
            }
        });
        resignButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                drawOffer = false;
                ReadInput r = new ReadInput();
                r.callfromReplay = false;
                r.setFlippy(flippy);
                r.readInput("resign");
            }
        });
        undoButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                drawOffer = false;
                ReadInput r = new ReadInput();
                r.callfromReplay = false;
                r.setFlippy(flippy);
                r.readInput("undo");
            }
        });
        drawButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                System.out.println("before");
                System.out.println("drawoffer: " + drawOffer);
                System.out.println("drawwaspressed: " +drawWasPressed );
                System.out.println("board.turnDraw: " + board.turnDraw);
                System.out.println("turns: " +turns );
                System.out.println("turnDrawWasPressed: " +turnDrawWasPressed );
                System.out.println("after");
                if (drawOffer && !drawWasPressed && board.turnDraw == turns) {
                    ReadInput r = new ReadInput();
                    r.callfromReplay = false;
                    r.setFlippy(flippy);
                    r.readInput("draw");
                } else {
                    if(!drawWasPressed) {
                        drawWasPressed = true;
                        drawOffer = true;
                        turnDrawWasPressed = getTurns();
                        textDisplay.append(" draw will be offered");
                    }
                }
                System.out.println("drawoffer: " + drawOffer);
                System.out.println("drawwaspressed: " +drawWasPressed );
                System.out.println("board.turnDraw: " + board.turnDraw);
                System.out.println("turns: " +turns );
                System.out.println("turnDrawWasPressed: " +turnDrawWasPressed );
            }
        });
        board.printBoard();
    }

    private void replayGame() throws IOException, ClassNotFoundException {
        turns = 0;
        drawWasPressed = false;
        turnDrawWasPressed = 0;
        drawOffer = false;
        flippy.setDisplayedChild(2);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flippy.setDisplayedChild(0);
                toolbar.setNavigationIcon(null);
                toolbar.getMenu().clear();
                return;
            }
        });

        final ListView lv = (ListView) flippy.findViewById(R.id.listView);
        try {
            //Read from disk using FileInputStream
            FileInputStream f_in = context.openFileInput("games.ser");
            obj_in = new ObjectInputStream(f_in);
            //Read an object
            Object obj = obj_in.readObject();
            if (obj instanceof ArrayList) {
                gameList = (ArrayList<GameRecordArray>) obj;
            }
            f_in.close();
            obj_in.close();
        } catch (Exception e) {
            System.out.println(e);
        }

        if (gameList == null || gameList.isEmpty()) {
            ArrayList<String> empArray = new ArrayList<String>();
            empArray.add("No save games");
            lv.setAdapter(new ArrayAdapter<String>(mActivity, R.layout.listitem, empArray));
            return;
        }

        final ArrayList<String> gameTitleList = new ArrayList<>();
        for (GameRecordArray GRA : gameList) {
            gameTitleList.add(GRA.getTitle() + "\n" + GRA.date);
        }
        //set list view to game titles
        final ListAdapter adapter = new ArrayAdapter<String>(mActivity, R.layout.listitem, gameTitleList);
        lv.setAdapter(adapter);
        lv.setSelection(0);

        Menu menu = toolbar.getMenu();
        MenuInflater inflater = mActivity.getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        MenuItem menuItemDate = menu.findItem(R.id.sortDate);
        MenuItem menuItemABC = menu.findItem(R.id.sortABC);

        menuItemABC.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                if (!Game.abcClick){
                    Collections.sort(gameTitleList, String.CASE_INSENSITIVE_ORDER);
                    ((BaseAdapter) adapter).notifyDataSetChanged();
                } else {
                    Collections.sort(gameTitleList, Collections.reverseOrder(String.CASE_INSENSITIVE_ORDER));
                    ((BaseAdapter) adapter).notifyDataSetChanged();
                }
                Game.abcClick = !Game.abcClick;
                return true;
            }
        });

        menuItemDate.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                Collections.sort(gameTitleList, new Comparator<String>() {
                    @Override
                    public int compare(String s1, String s2) {
                        String[] sa1 = s1.split("\n");
                        String[] sa2 = s2.split("\n");
                        String sd1 = sa1[1];
                        String sd2 = sa2[1];
                        SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
                        Date date1 = null;
                        Date date2 = null;
                        try {
                            date1 = formatter.parse(sd1);
                            date2 = formatter.parse(sd2);
                        } catch(ParseException e){ System.out.println(e);}
                        if(Game.dateClick) {
                            if (date1.after(date2)) {
                                return 1;
                            }
                            if (date1.before(date2)) {
                                return -1;
                            }
                        } else {
                            if (date1.after(date2)) {
                                return -1;
                            }
                            if (date1.before(date2)) {
                                return 1;
                            }
                        }
                        return 0;
                    }
                });
                Game.dateClick = !Game.dateClick;
                ((BaseAdapter) adapter).notifyDataSetChanged();
                return true;
            }
        });

        lv.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView,
                                            View view, int i, long l) {
                        flippy.setDisplayedChild(3);
                        toolbar.getMenu().clear();
                        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
                        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                flippy.setDisplayedChild(0);
                                toolbar.setNavigationIcon(null);
                                return;
                            }
                        });
                        TextView replayText = (TextView) flippy.findViewById(R.id.replayTextView);
                        replayText.setText("White's Move");
                        final Button nextButton = (Button) flippy.findViewById(R.id.nextButton);
                        nextButton.setEnabled(true);
                        String s = (String) lv.getItemAtPosition(i);
                        String sa[] = s.split("\n");
                        ArrayList<String> replayMovestemp = null;
                        for (GameRecordArray GRA : gameList) {
                            if (GRA.getTitle().equalsIgnoreCase(sa[0])) {
                                replayMovestemp = GRA.getListOfMoves();
                                break;
                            }
                        }
                        final ArrayList<String> replayMoves = replayMovestemp;

                        board = new Board(white, black, mActivity, context, flippy, toolbar);
                        board.callFromReplay = true;
                        board.printBoard();
                        nextButton.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                String move = replayMoves.remove(0);
                                ReadInput readIn = new ReadInput();
                                readIn.callfromReplay = true;
                                readIn.undoLastTurn = false;
                                readIn.setFlippy(flippy);
                                System.out.println(move +" " +turns );
                                readIn.readInput(move);
                                if (replayMoves.size() == 0) {
                                    nextButton.setEnabled(false);
                                }
                            }
                        });

                    }
                }
        );
    }

    /**
     * @return board object of the game
     */
    public static Board getBoard() {
        return board;
    }

    /**
     * @return lightsquare player of the game
     */
    public static Player getPlayerW() {
        return white;
    }

    /**
     * @return blacksquare player of the game
     */
    public static Player getPlayerB() {
        return black;
    }

    /**
     * @return number of turns taken
     */
    public static int getTurns() {
        return turns;
    }

    /**
     * @return current player of the game as a boolean, true for lightsquare, false for blacksquare
     */
    public static boolean getCurrentPlayer() {
        // White
// blacksquare
        return Game.getTurns() % 2 == 0;
    }
}


