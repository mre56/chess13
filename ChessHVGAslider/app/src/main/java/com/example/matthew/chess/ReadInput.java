package com.example.matthew.chess;

import android.app.Dialog;
import android.content.ClipData;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;
import android.widget.ImageView.ScaleType;
/**
 * This class takes the user input and decides if it is a valid move.
 *
 * @author Matthew Eder & Alex Weinrich
 */
public class ReadInput {
    /**
     * checked by game to see if input of valid
     */
    boolean valid = false;
    /**
     * keeps draw whether a draw was offered last turn
     */

    static ViewFlipper flippy = null;
    static boolean callfromReplay = false;
    static Board board;
    String input;
    static String prevMove = "";
    static Piece captured = null;
    static boolean undoLastTurn = false;
    static boolean wasEnpassant = false;
    static String promoteString;
    static boolean prevMoveWasPromote = false;

    public ReadInput() {
    }

    /**
     * Process the input given by the user to tell whether the input is in a valid format
     * then in if the move is a valid chess move. It will change the gameOver state in Game
     * class if a draw is accepted or player resigns
     *
     * @param input The move the user inputs
     */
    public boolean readInput(String input) {
        this.input = input;
        String pattern = "([a-h][1-8]\\s+[a-h][1-8](\\s+(draw\\?|N|Q|R|B))?|draw|resign|ai|undo)\\s*";
        TextView text;
        board = Game.getBoard();
        if (callfromReplay) {
            text = (TextView) flippy.findViewById(R.id.replayTextView);
        } else {
            text = (TextView) flippy.findViewById(R.id.textView);
        }
        if (!input.matches(pattern)) {
            text.setText("Illegal move, try again");
            return false;
        } else {
            String[] inputArray = input.split(" ");

            String init; // initial position or resign or draw
            init = inputArray[0];

            if (init.equals("resign")) {
                valid = true;
                Game.gameOver = true;
                if (Game.getCurrentPlayer())
                    text.setText("White resigns");
                else
                    text.setText("Black resigns");
                if (!callfromReplay) {
                    Game.GRArray.getListOfMoves().add(input);
                    board.endGamePrompt();
                }
                return true;
            }

            if (init.equals("draw")) {
                valid = true;
                Game.draw = true;
                Game.gameOver = true;
                text.setText("Draw accepted");
                if (!callfromReplay) {
                    Game.GRArray.getListOfMoves().add(input);
                    board.endGamePrompt();
                }
                return true;
            }

            if (init.equalsIgnoreCase("ai")) {
                new FindMove();
                //board.printBoard();
                //valid = true;
                return true;
            }

            if (init.equalsIgnoreCase("undo")) {
                if (!undoLastTurn && Game.turns != 0) {
                    String[] prevMoveArray = prevMove.split(" ");
                    final int fromY = 8 - Character.getNumericValue(prevMoveArray[0].charAt(1));
                    final int fromX = (int) prevMoveArray[0].charAt(0) - 97;  // ascii value of 'a' is 97
                    final int toY = 8 - Character.getNumericValue(prevMoveArray[1].charAt(1));
                    final int toX = (int) prevMoveArray[1].charAt(0) - 97;

                    Piece prevMovingPiece = board.getPosition(toX, toY).clearPosition();

                    if(prevMovingPiece instanceof  King && ((King) prevMovingPiece).prevMoveWasCastle) {
                        Piece rook = null;
                        ((King) prevMovingPiece).prevMoveWasCastle = false;
                        ((King) prevMovingPiece).hasMoved = false;
                        if(prevMovingPiece.getPlayer().getColor()) {
                            if(toX == 2){
                                rook = board.getPosition(toX+1, 7).clearPosition();
                                board.getPosition(0, 7).setPosition(rook);
                            } else {
                                rook = board.getPosition(toX-1, 7).clearPosition();
                                board.getPosition(7, 7).setPosition(rook);

                            }
                        } else {
                            if(toX == 2){
                                rook = board.getPosition(toX+1, 0).clearPosition();
                                board.getPosition(0, 0).setPosition(rook);
                            } else {
                                rook = board.getPosition(toX-1, 0).clearPosition();
                                board.getPosition(7, 0).setPosition(rook);
                            }
                        }
                        ((Rook) rook).hasMoved = false;
                    }

                    if(prevMoveWasPromote) {
                        board.getPosition(fromX, fromY).setPosition(new Pawn(new Player(!Game.getCurrentPlayer())));
                    } else {
                        board.getPosition(fromX, fromY).setPosition(prevMovingPiece);
                    }

                    if(captured != null && !wasEnpassant) {
                        board.getPosition(toX, toY).setPosition(captured);
                    } else if(captured != null && wasEnpassant){
                        board.getPosition(toX, fromY).setPosition(captured);
                    }

                    board.printBoard();
                    if (!callfromReplay) {
                        Game.GRArray.getListOfMoves().add("undo");
                    }
                    valid = true;
                    undoLastTurn = true;
                    Game.turns++;
                    if (Game.getCurrentPlayer())
                        text.setText("White's move");
                    else
                        text.setText("Black's move");
                    return true;
                } else {
                    if (Game.getCurrentPlayer())
                        text.setText("White's move");
                    else
                        text.setText("Black's move");
                    text.append(", can't undo");
                    return true;
                }
            }

            String end; //ending position
            end = inputArray[1];

            if (inputArray.length > 2) {
                String optionalArg = inputArray[2];
                if (optionalArg.equalsIgnoreCase("draw?")) {
                    Game.drawOffer = true;
                }
            } else {
                Game.drawOffer = false;
            }

            int fromX, fromY, toX, toY;
            fromY = 8 - Character.getNumericValue(init.charAt(1));
            fromX = (int) init.charAt(0) - 97;  // ascii value of 'a' is 97

            Position start = board.getPosition(fromX, fromY);

            if (start.getPiece() != null && start.getPiece().getPlayer().getColor() == Game.getCurrentPlayer()) {
                Piece piece = start.getPiece();
                toY = 8 - Character.getNumericValue(end.charAt(1));
                toX = (int) end.charAt(0) - 97;
                wasEnpassant = false;
                if (piece.isValid(board, fromX, fromY, toX, toY)) {
                    // Test for pawn promotion
                    if ((piece instanceof Pawn) && (((toY == 0) &&
                            (board.getPosition(fromX, fromY).getPiece().getPlayer().getColor())) ||
                            ((toY == 7) && (!board.getPosition(fromX, fromY).getPiece().getPlayer().getColor())))) {
                        try {
                            String promoteString = inputArray[2];
                            pawnPromotionReplay(fromX, fromY, toX, toY, promoteString);
                        } catch (IndexOutOfBoundsException e) {
                            prevMoveWasPromote = true;
                            pawnDialouge(board, fromX, fromY, toX, toY);
                        }
                        return true;
                    } else {
                        Position finish = board.getPosition(toX, toY);
                        if(!wasEnpassant) {
                            captured = finish.clearPosition();
                        }
                        finish.setPosition(piece);
                        start.clearPosition();
                        valid = true;
                        if (callfromReplay) {
                            setPieceImages(fromX, fromY, toX, toY);
                        } else {
                            Game.GRArray.getListOfMoves().add(input);
                            undoLastTurn = false;
                        }
                        prevMove = input;
                        return true;
                    }
                } else {
                    text.setText("Illegal move, try again ");
                    if (Game.getCurrentPlayer())
                        text.append("White's move");
                    else
                        text.append("Black's move");
                    return false;
                }

            } else {
                text.setText("Illegal move, try again ");
                if (Game.getCurrentPlayer())
                    text.append("White's move");
                else
                    text.append("Black's move");
                return false;
            }
        }
    }

    public void setFlippy(ViewFlipper flippy) {
        this.flippy = flippy;
    }

    public void setPieceImages(int fromX, int fromY, int toX, int toY) {
        GridView gridView = (GridView) flippy.findViewById(R.id.gridView2);
        ViewGroup owner = (ViewGroup) gridView.getChildAt(fromX + (fromY * 8));
        RelativeLayout container = (RelativeLayout) gridView.getChildAt(toX + (toY * 8));
        View view = owner.getChildAt(1);
        owner.removeView(view);
        if (container.getChildAt(1) != null) {
            container.removeView(container.getChildAt(1));
        }
        container.addView(view);
        board.boardChecker();
    }

    /** Method to handle all functionality of promoting a pawn. Asks the user
     for input regarding what to promote the pawn to and loops until proper input
     is given, then casts a Piece to the proper type and inserts it into the
     location the pawn used to hold after clearing the pawn from the board.
     *
     * @param fromX used to indicate where the pawn is moving from X, to place the new piece
     * @param fromY used to indicate where the pawn is moving from Y, to place the new piece
     * @return false if it fails true if it works
     */
    public void pawnPromotion(int fromX, int fromY, int toX, int toY) {
        //Boolean to determine if the loop asking for input should be left or not
        //Temporary piece to be cast to intended promo piece target
        captured = board.getPosition(toX, toY).clearPosition();

        Piece promoPiece = new Piece(board.getPosition(fromX, fromY).getPiece().getPlayer());

        switch (promoteString) {
            case "Q":
                promoPiece = new Queen(board.getPosition(fromX, fromY).getPiece().getPlayer());
                break;
            case "R":
                promoPiece = new Rook(board.getPosition(fromX, fromY).getPiece().getPlayer());
                break;
            case "B":
                promoPiece = new Bishop(board.getPosition(fromX, fromY).getPiece().getPlayer());
                break;
            case "N":
                promoPiece = new Knight(board.getPosition(fromX, fromY).getPiece().getPlayer());
                break;
        }
        //Clears the pawn, gets its old position, and sets the promoPiece to that position
        Position promoPos1 = board.getPosition(fromX, fromY);
        Position promoPos2 = board.getPosition(toX, toY);
        promoPos1.clearPosition();
        promoPos2.setPosition(promoPiece);
        board.printBoard();
        board.boardChecker();
        valid = true;
        if (!callfromReplay) {
            Game.GRArray.getListOfMoves().add(input + " " + promoteString);
            undoLastTurn = false;
        }
        prevMove = input;
    }

    public void pawnDialouge(Board board, final int fromX,final int fromY,final int toX, final int toY) {
        final Dialog promoteDialoge = new Dialog(Game.mActivity);
        promoteDialoge.setTitle("Pawn promotion");
        promoteDialoge.setContentView(R.layout.promotion);
        promoteDialoge.setCanceledOnTouchOutside(false);
        promoteDialoge.show();

        TextView p = (TextView) promoteDialoge.findViewById(R.id.promoteText);
        p.setKeyListener(null);
        ImageView knightPromote = (ImageView) promoteDialoge.findViewById(R.id.knightPromote);
        ImageView queenPromote = (ImageView) promoteDialoge.findViewById(R.id.queenPromote);
        ImageView rookPromote = (ImageView) promoteDialoge.findViewById(R.id.rookPromote);
        ImageView bishopPromote = (ImageView) promoteDialoge.findViewById(R.id.bishopPromote);
        if(Game.getCurrentPlayer()) {
            knightPromote.setImageResource(R.drawable.wn);
            queenPromote.setImageResource(R.drawable.wq);
            rookPromote.setImageResource(R.drawable.wr);
            bishopPromote.setImageResource(R.drawable.wb);
        } else {
            knightPromote.setImageResource(R.drawable.bn);
            queenPromote.setImageResource(R.drawable.bq);
            rookPromote.setImageResource(R.drawable.br);
            bishopPromote.setImageResource(R.drawable.bb);
        }
        knightPromote.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                promoteString = "N";
                pawnPromotion(fromX, fromY, toX, toY);
                promoteDialoge.dismiss();
                return true;
            }
        });
        queenPromote.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                promoteString = "Q";
                pawnPromotion(fromX, fromY, toX, toY);
                promoteDialoge.dismiss();
                return true;
            }
        });
        rookPromote.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                promoteString = "R";
                pawnPromotion(fromX, fromY, toX, toY);
                promoteDialoge.dismiss();
                return true;
            }
        });
        bishopPromote.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                promoteString = "B";
                pawnPromotion(fromX, fromY, toX, toY);
                promoteDialoge.dismiss();
                return true;
            }
        });
    }

    public void pawnPromotionReplay(int fromX, int fromY, int toX, int toY, String promo) {
        //Boolean to determine if the loop asking for input should be left or not
        //Temporary piece to be cast to intended promo piece target
        captured = board.getPosition(toX, toY).clearPosition();

        Piece promoPiece = new Piece(board.getPosition(fromX, fromY).getPiece().getPlayer());

        switch (promo) {
            case "Q":
                promoPiece = new Queen(board.getPosition(fromX, fromY).getPiece().getPlayer());
                break;
            case "R":
                promoPiece = new Rook(board.getPosition(fromX, fromY).getPiece().getPlayer());
                break;
            case "B":
                promoPiece = new Bishop(board.getPosition(fromX, fromY).getPiece().getPlayer());
                break;
            case "N":
                promoPiece = new Knight(board.getPosition(fromX, fromY).getPiece().getPlayer());
                break;
        }
        //Clears the pawn, gets its old position, and sets the promoPiece to that position
        Position promoPos1 = board.getPosition(fromX, fromY);
        Position promoPos2 = board.getPosition(toX, toY);
        promoPos1.clearPosition();
        promoPos2.setPosition(promoPiece);
        valid = true;
        if (!callfromReplay) { // call from ai
            Game.GRArray.getListOfMoves().add(input);
            undoLastTurn = false;
        } else {
            Game.turns++; //I need it to find why
        }
        prevMove = input;
        board.printBoard();
        board.boardChecker();
    }
}