Author: Matthew Eder

Rutgers University CS213 Software Methodology spring 2016

Chess App

# README #

- only tested on 3.2 inch HVGA slider (ADP1) (320 x 480: mdpi) 

Extras

- stalemate

known issues

- Enpassant(in general(says is illegal sometimes) and undo)

- Promotion (no dialogue window, some pawns cant reach end(sometimes))

- undo AI move

After late updates

- fixed enpassant

- ported castling 

- ported promotion

- fixed ports to draw

- fixed undo for everything

- ai promotion

- sorting in reverse orders

- non case sensitive sorting

- two sort buttons in action bar for date and abc

- in game text display improvements

- nexus 5x version (phone I own)

- stuff added for any screen size compatibility 

to do list

- single player

- unlimited undos

- more screen size compatibility

- AI move selection logic